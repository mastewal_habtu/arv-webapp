from judges.views import is_judge
from manage_tasks.models import Task
from wizards.views import is_wizard


def new_info_count_notification(request):
    if is_judge(request.user):
        judge = request.user
        # unfinished tasks to a user
        unfinished_tasks = judge.judge_tasks.filter(judgeassignment__state__lt=Task.FINISHED)
        unfinished_tasks_count = unfinished_tasks.count()

        # new assigned tasks to a user
        new_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.ASSIGNED_NEW)
        new_tasks_count = new_tasks.count()

        # new feededback tasks to a user
        new_feededback_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.FEEDEDBACK_NEW)
        new_feededback_tasks_count = new_feededback_tasks.count()

        # new expired tasks to a user
        new_expired_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.EXPIRED_NEW)
        new_expired_tasks_count = new_expired_tasks.count()

        # context that contains count info to be used in template
        context = {
            "unfinished_tasks_count": unfinished_tasks_count,
            "new_tasks_count": new_tasks_count,
            "new_feededback_tasks_count": new_feededback_tasks_count,
            "new_expired_tasks_count": new_expired_tasks_count,
        }
    elif is_wizard(request.user):
        wizard = request.user
        # all finished tasks to a user
        unfinished_tasks = wizard.wizard_tasks.filter(wizardassignment__state__lt=Task.FINISHED)
        unfinished_tasks_count = unfinished_tasks.count()

        # new assigned tasks to a user
        new_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.ASSIGNED_NEW)
        new_tasks_count = new_tasks.count()

        # new feededback tasks to a user
        new_feededback_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.FEEDEDBACK_NEW)
        new_feededback_tasks_count = new_feededback_tasks.count()

        # new expired tasks to a user
        new_expired_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.EXPIRED_NEW)
        new_expired_tasks_count = new_expired_tasks.count()

        # context that contains count info to be used in template
        context = {
            "unfinished_tasks_count": unfinished_tasks_count,
            "new_tasks_count": new_tasks_count,
            "new_feededback_tasks_count": new_feededback_tasks_count,
            "new_expired_tasks_count": new_expired_tasks_count,
        }

    else:
        context = {}

    return context
