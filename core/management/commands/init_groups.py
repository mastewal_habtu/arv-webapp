from django.core.management import BaseCommand
from django.contrib.auth.models import Group
from django.db.utils import IntegrityError


# The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):
    """
    A command to create the necessary groups for the web application
    """

    # Show this when the user types help
    help = "A command to create the necessary groups for the web application"

    # A command must define handle()
    def handle(self, *args, **options):
        # create the necessary groups
        # beware there will integrity error if one of them already exists

        # create "Wizards" group if it doesn't exist
        try:
            Group.objects.create(name="Wizards")
            self.stdout.write("Wizards Group created.\n")
        except IntegrityError:
            self.stdout.write("Wizards Group is not created.")
            self.stdout.write("A group already with a name Wizards exists.\n")

        # create "Judges" group if it doesn't exist
        try:
            Group.objects.create(name="Judges")
            self.stdout.write("Judges Group created.\n")
        except IntegrityError:
            self.stdout.write("Judges Group is not created.")
            self.stdout.write("A group already with a name Judges exists.\n")
