from django.db import models
from django.conf import settings

from django.utils import timezone

import uuid
import os
import pytz

"""
all timezones from pytz library as pairs
"""
all_timezones = zip(pytz.all_timezones, pytz.all_timezones)


def user_profile_picture_path(instance, filename):
    """
    Generate a path for a profile picture storage with unique filename

    Args:
        instance: don't know it
        filename: filename of uploaded file

    Returns:
        A path including filename relative to the media folder that will be used to store the uploaded file
    """

    # get the extension of the file
    ext = filename.split('.')[-1]

    # every user's profile picture will be stored in his/her own folder that is named using user's id
    folder_name = "user_{0}".format(instance.user.id)
    filename = "{0}.{1}".format(uuid.uuid4(), ext)

    return os.path.join(settings.PROFILE_PICTURE_FOLDER, folder_name, filename)


class Profile(models.Model):
    """
    Profile model that consists additional information for a specific user
    """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    contact_no = models.CharField(max_length=15, blank=True)
    picture = models.ImageField(upload_to=user_profile_picture_path, default="anonymous.png", unique=False)
    timezone = models.CharField(max_length=255, choices=all_timezones, default=timezone.get_current_timezone_name())

    # number of tasks finished by the user
    finished_tasks = models.PositiveIntegerField(default=0)
    # number of tasks expired
    expired_tasks = models.PositiveIntegerField(default=0)
    # number of tasks that are successfully predicted by the user
    success_tasks = models.PositiveIntegerField(default=0)
    # number of tasks that are currently assigned and being done by the user
    active_tasks = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "Profile of {}".format(self.user.username)
