import re

from django import template
from django.shortcuts import reverse, NoReverseMatch


register = template.Library()


@register.simple_tag(takes_context=True)
def active_if(context, *pattern_or_url_names):
    path = context['request'].path

    for pattern_or_url in pattern_or_url_names:
        try:
            pattern = "^" + reverse(pattern_or_url)
        except NoReverseMatch:
            pattern = pattern_or_url

        if re.search(pattern, path):
            return 'active'

    return ''
