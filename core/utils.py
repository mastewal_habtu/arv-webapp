import pickle
import uuid
import os
import pytz

from django.contrib.auth.models import Group
from django.contrib.auth import update_session_auth_hash
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseBadRequest
from django.shortcuts import redirect

from arv import settings
from core.models import Profile
from core.forms import EditUserForm, ProfileForm
from manage_tasks.models import Task, JudgeAssignment, WizardAssignment


def is_admin(user):
    """
    Check if user is admin and also the user has logged in
    In current case, a judge is an admin, need to check if user is judge only

    Args:
          user: the current user in the http request

    Returns:
          A boolean indicating if user is logged in and is an admin(judge)
    """

    if user.is_authenticated:
        return user.groups.filter(name="judges").exists()
    else:
        return False


def update_user_from_form(request, user_form, profile_form, role_change=True):
    """
    A logic to update a specific user based on submitted form data

    Args:
        request: the http request
        user_form: the user data populated UserForm instance
        profile_form: the user data populated ProfileForm instance
        role_change: A boolean indicating if a role change is allowed to be changed or not

    Returns:
        a boolean indicating if the user is updated or not
    """
    # if forms are valid, update user with its profile
    if user_form.is_valid() and profile_form.is_valid():
        # update user from form
        new_user = user_form.save(commit=False)
        # set new password if new password is given
        if user_form.fields['change_password'].has_changed("", user_form.cleaned_data['change_password']):
            new_user.set_password(user_form.cleaned_data['change_password'])
            # update session since password change will invalidate user and force log out
            update_session_auth_hash(request, new_user)
        new_user.save()

        # update profile from form and associate the user with it
        new_profile = profile_form.save(commit=False)
        new_profile.user = new_user
        new_profile.save()

        # assign user to appropriate group
        if role_change:
            new_user.groups.clear()
            group = Group.objects.get(name=user_form.cleaned_data['role'])
            group.user_set.add(new_user)

        return True

    # if one of the forms is invalid
    return False


def get_user_profile(user):
    """
    Get user profile, and assign new one if user has no profile

    Args:
        user: the user instance for which his/her profile will be requested for

    Returns:
        the profile instance related to the user,
        a new default profile could be created and associated with the user if user has no profile
    """

    # check if user has profile to avoid exception
    # and create new profile if it don't have
    if hasattr(user, 'profile'):
        profile = user.profile
    else:
        profile = Profile()
        profile.user = user
        profile.save()

    return profile


def get_user_group(user):
    """
    Get user group i.e. user role

    Args:
        user: the user instance for which his/her group will be requested for

    Returns:
        The name of the first group the user is in or
        "Has no role" string if the user is not in any group at all
    """

    # check if user has at least one group
    # and choose the first group_name if found
    if user.groups.all().count() >= 1:
        group_name = user.groups.all()[0].name
    else:
        group_name = "Has no role"

    return group_name


def edit_user_logic(request, user, role_change=True):
    """
    A logic for editing a specific user and his/her profile

    Args:
        request: the http request
        user: the user instance to be edited
        role_change: A boolean indicating if a role change is allowed to be changed or not

    Returns:
        An http redirect if editing user was successful or
        a context variable i.e dictionary for template usage otherwise
    """

    # get user profile and assign new one if user has no profile
    profile = get_user_profile(user)

    # get user group i.e user role
    group_name = get_user_group(user)

    # if form is submitted
    if request.method == "POST":
        # populate both forms from submitted data and current model
        user_form = EditUserForm(request.POST, instance=user)
        profile_form = ProfileForm(request.POST, request.FILES, instance=profile)

        is_updated = update_user_from_form(request, user_form, profile_form, role_change)

        # save the success info if the update was successful
        request.session['updated'] = is_updated
        # save the failure info if the update was unsuccessful
        request.session['not_updated'] = not is_updated
        # save the errors that could arise from form submission
        request.session['user_form_errors'] = user_form.errors
        request.session['profile_form_errors'] = profile_form.errors

        # redirect to itself
        return redirect(request.path)
    else:  # if request method is GET
        # populate form using current model
        user_form = EditUserForm(instance=user)
        profile_form = ProfileForm(instance=profile)

    # check if there was successful update and invalidate it
    success = get_status(request, "updated", False)
    # check if there was unsuccessful update and invalidated it
    failure = get_status(request, "not_updated", False)
    # get errors if there are any and invalidate them
    user_form_errors = get_status(request, "user_form_errors", {})
    profile_form_errors = get_status(request, "profile_form_errors", {})

    # context variables for template
    context = {
        "user_form": user_form,
        "profile_form": profile_form,
        "group": group_name,
        "timezones": pytz.common_timezones,
        "success": success,
        "failure": failure,
        "user_form_errors": user_form_errors,
        "profile_form_errors": profile_form_errors,
    }

    return context


def get_paginated_items(request, items_list, items_per_page=10):
    """
    Get items in a paginated form (10 items per page is the default)

    Args:
        request: http request
        items_list: all the items that are going to be paginated
        items_per_page: how many items should be in a single page

    Returns:
        the items in current page, current page is found from url
    """

    paginator = Paginator(items_list, items_per_page)

    # get page number from query
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # if page is not integer, deliver first page
        items = paginator.page(1)
    except EmptyPage:
        # if page is out of range, deliver last page
        items = paginator.page(paginator.num_pages)

    return items


def get_pages(items, limit=5):
    """
    Get a number of pages for pagination

    Args:
         items: the items on the current page
         limit: how many number of pages should be returned

    Returns:
        the page ranges to be used for pagination
        if the pages are more than limit or not
    """

    if items.paginator.num_pages <= limit:
        return items.paginator.page_range, False

    return items.paginator.page_range[items.number - 1:limit], True


def ajax_required(fun):
    """
    A decorator function that checks if request is sent using ajax or not

    Args:
        fun: the function to be decorated, this function first parameter should be request object

    Returns:
        the argument function itself if ajax request was sent, or HttpResponseBadRequest
        otherwise
    """

    def wrap(request, *args, **kwargs):
        """
        the wrapper function

        Args:
            request: http request
            args: variable argument list if there are arguments
            kwargs:  arbitrary keyword argument if there are keyword arguments

        Returns:
            fun with its corresponding arguments or HttpResponseBadRequest if request is
            not ajax
        """
        if not request.is_ajax():
            return HttpResponseBadRequest()

        return fun(request, *args, **kwargs)

    wrap.__doc__ = fun.__doc__
    wrap.__name__ = fun.__name__

    return wrap


def unique_filename(path):
    """
    Creates new function that handle giving a unique filename for uploaded file

    Args:
        path: a folder path

    Returns:
        a function that handles giving a unique filename for uploaded file using given path
    """

    def wrap(instance, filename):
        """
        Generate a path for an uploaded file storage with unique filename

        Args:
            instance: don't know it
            filename: filename of uploaded file

        Returns:
            A path including filename relative to the media folder that will be used to store the uploaded file
        """

        # get the extension of the file
        ext = filename.split('.')[-1]

        filename = "{0}.{1}".format(uuid.uuid4(), ext)

        return os.path.join(path, filename)

    return wrap


def get_status(request, key, default):
    """
    Get a stored value in the current session and change it to the default
    If no stored value can not be found, return default

    Args:
        request: current http request from where the session is found
        key: a session key to store and retrieve value from the session
        default: a default value to be used to reset the session value and when no value is found

    Returns:
        A value found from the session or the default value if no session value is found
    """

    value = request.session.get(key, default)
    request.session[key] = default

    return value


# N.B. This method is deprecated
def get_items_with_pages(request, items_list, items_per_page=10, page_limit=5):
    """
    get paginated items with pages
    """
    items = get_paginated_items(request, items_list, items_per_page)  # 10 tasks per page
    pages = get_pages(items, page_limit)  # pagination shown with maximum 5 pages

    return items, pages


def log_failure(dictionary, log_file_name=settings.FAILURE_LOG_FILE_NAME):
    """
    Log to the file using pickling a list of dictionaries

    Args:
        dictionary: a dictionary to be appended to the log file
        log_file_name: the filename where the log is written and read,
                       defaults to value of FAILURE_LOG_FILE_NAME set in settings

    Returns:
        nothing
    """
    try:
        with open(log_file_name, 'rb') as log_file_in:
            logged_failures = pickle.load(log_file_in)
    except FileNotFoundError:
        logged_failures = []

    logged_failures.append(dictionary)

    with open(log_file_name, 'wb') as log_file_out:
        pickle.dump(logged_failures, log_file_out)


def get_unfinished_tasks(user, is_wizard=False, is_judge=False):
    if is_wizard and is_judge:
        raise AssertionError("A user can not be both wizard and judge")

    if is_wizard:
        return user.wizard_tasks.filter(wizardassignment__state__lt=Task.FINISHED).count()
    elif is_judge:
        return user.judge_tasks.filter(judgeassignment__state__lt=Task.FINISHED).count()
    else:
        raise AssertionError("A user should be a wizard or judge")


def get_finished_tasks(user, **kwargs):
    if kwargs.get('is_wizard') and kwargs.get('is_judge'):
        raise AssertionError("A user can not be both wizard and judge")

    if kwargs.get('is_wizard'):
        return user.wizard_tasks.order_by('wizardassignment__state') \
            .filter(wizardassignment__state__gte=Task.FINISHED) \
            .exclude(wizardassignment__state__gte=Task.EXPIRED_NEW) \
            .count()
    elif kwargs.get('is_judge'):
        return user.judge_tasks \
            .filter(judgeassignment__state__gte=Task.FINISHED) \
            .exclude(judgeassignment__state__gte=Task.EXPIRED_NEW) \
            .count()
    else:
        raise AssertionError("A user should be a wizard or judge")


def get_expired_tasks(user, **kwargs):
    if kwargs.get('is_wizard') and kwargs.get('is_judge'):
        raise AssertionError("A user can not be both wizard and judge")

    if kwargs.get('is_wizard'):
        return user.wizard_tasks.order_by('wizardassignment__state') \
            .filter(wizardassignment__state__gte=Task.EXPIRED_NEW) \
            .count()
    elif kwargs.get('is_judge'):
        return user.judge_tasks \
            .filter(judgeassignment__state__gte=Task.EXPIRED_NEW) \
            .count()
    else:
        raise AssertionError("A user should be a wizard or judge")


def get_success_tasks(user, **kwargs):
    if kwargs.get('is_wizard') and kwargs.get('is_judge'):
        raise AssertionError("A user can not be both wizard and judge")

    if kwargs.get('is_wizard'):
        correct, incorrect, total = 0, 0, 0
        for wizard_assignment in WizardAssignment.objects.filter(wizard=user).exclude(state__gte=Task.EXPIRED_NEW):
            if wizard_assignment.task.event is not None:
                total += 1
                is_correct = wizard_assignment.is_correct()
                if is_correct is True:
                    correct += 1
                elif is_correct is False:
                    incorrect += 1
        return [correct, incorrect, total]
    elif kwargs.get('is_judge'):
        correct, incorrect, total = 0, 0, 0
        for judge_assignment in JudgeAssignment.objects.filter(judge=user).exclude(state__gte=Task.EXPIRED_NEW):
            result = judge_assignment.get_feedback()
            correct += result[0]
            incorrect += result[1]
            total += result[2]
        return [correct, incorrect, total]
    else:
        raise AssertionError("A user should be a wizard or judge")
