from django import forms
from django.contrib.auth import get_user_model
from django.forms import ModelForm
from django.contrib.auth.models import User

from core.models import Profile

# possible groups in the system with their friendly name
groups = [
    ("wizards", "Wizard"),
    ("judges", "Judge"),
]


class ImageForm(forms.Form):
    """
    A form to only check if uploaded image is valid
    """

    image = forms.ImageField()


class EditUserForm(ModelForm):
    """
    A form to edit user information excluding profile information
    """

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    role = forms.ChoiceField(choices=groups, required=True)
    change_password = forms.CharField(max_length=100, required=False)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']


class ProfileForm(ModelForm):
    """
    A form to edit profile information of a user
    """

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    class Meta:
        model = Profile
        fields = ['contact_no', 'picture', 'timezone']


class TimezoneForm(ModelForm):
    """
    A form to change a timezone of a user
    """

    def __init__(self, *args, **kwargs):
        super(TimezoneForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    class Meta:
        model = Profile
        fields = ['timezone']


class UserForm(ModelForm):
    """
    A form for adding new user
    """

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    role = forms.ChoiceField(choices=groups, required=True)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

    def clean_email(self):
        email = self.cleaned_data['email']

        if not email:
            raise forms.ValidationError("An email is required")

        if get_user_model().objects.filter(email=email).exists():
            raise forms.ValidationError("A user with the same email already exists.")

        return email