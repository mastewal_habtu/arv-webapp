from django.contrib.auth import logout, get_user_model, login
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from core.tokens import account_activation_token
from core.utils import is_admin, edit_user_logic, get_user_profile, get_finished_tasks, get_expired_tasks, \
    get_unfinished_tasks, get_success_tasks
from judges.views import is_judge
from manage_images.models import Image
from manage_tasks.models import Task
from wizards.views import is_wizard


@login_required
def role_login(request):
    """
    # login user based on the role of user
    """

    if is_wizard(request.user):
        return redirect(reverse('wizards:unfinished_tasks'))
    elif is_judge(request.user):
        return redirect(reverse('judges:unfinished_tasks'))
    else:
        return redirect(reverse("core:login"))


def logout_user(request):
    """
    # log a user out
    """

    logout(request)
    return redirect(reverse("core:login"))


@login_required
def self_profile(request):
    """
    A view to see details of user's own profile and edit his/her details

    Args:
        request: the http request

    Returns:
        An http redirect to itself if editing user failed or
        an html with appropriate messages if necessary
    """

    kwargs = {
        'is_wizard': is_wizard(request.user),
        'is_judge': is_judge(request.user)
    }

    finished_tasks = get_finished_tasks(request.user, **kwargs)
    expired_tasks = get_expired_tasks(request.user, **kwargs)
    unfinished_tasks = get_unfinished_tasks(request.user, **kwargs)
    success_tasks, unsuccess_tasks, total_tasks = get_success_tasks(request.user, **kwargs)

    success_tasks_percent = get_percent(success_tasks, total_tasks)
    if kwargs['is_wizard']:
        finished_tasks_percent = get_percent(finished_tasks, request.user.wizard_tasks.count())
        expired_tasks_percent = get_percent(expired_tasks, request.user.wizard_tasks.count())
        # active_tasks_percent = get_percent(unfinished_tasks, Task.objects.filter(wizardassignment__state__lte=Task.UNFINISHED).count())
    elif kwargs['is_judge']:
        finished_tasks_percent = get_percent(finished_tasks, request.user.judge_tasks.count())
        expired_tasks_percent = get_percent(expired_tasks, request.user.judge_tasks.count())
        # active_tasks_percent = get_percent(unfinished_tasks, Task.objects.filter(judgeassignment__state__lte=Task.UNFINISHED).count())
    else:
        return HttpResponseBadRequest()

    context = {
        "finished_tasks": finished_tasks,
        "finished_tasks_percent": finished_tasks_percent,
        "success_tasks": success_tasks,
        "success_tasks_percent": success_tasks_percent,
        "expired_tasks": expired_tasks,
        "expired_tasks_percent": expired_tasks_percent,
        "unfinished_tasks": unfinished_tasks,
        # "active_tasks_percent": active_tasks_percent,
    }

    # process logic for editing user and profile
    response = edit_user_logic(request, request.user, role_change=False)

    # if the response is http redirect, redirect
    # else the response is context variables for template
    if isinstance(response, HttpResponseRedirect):
        return response
    else:
        context.update(response)

    return render(request, "core/self_profile.html", context)




def confirm_and_activate(request, new_user_id, token):
    """
    Confirm a user email is right and activate his/her account

    Redirects to the homepage of the user
    """

    try:
        # user_id = force_text(urlsafe_base64_decode(user_id_b64))
        user_id = int(new_user_id)
        user = get_user_model().objects.get(pk=user_id)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

        login(request, user)

        return redirect("core:role_login")


@user_passes_test(is_admin)
def admin_dashboard(request):
    """
    Admin dashboard view for admin
    """

    tasks_count = Task.objects.count()
    images_count = Image.objects.count()
    users_count = get_user_model().objects.exclude(is_superuser=True).count()
    inactive_users_count = get_user_model().objects.filter(is_active=False).count()

    context = {
        "tasks_count": tasks_count,
        "images_count": images_count,
        "users_count": users_count,
        "inactive_users_count": inactive_users_count,
    }

    return render(request, "core/dashboard.html", context)


@user_passes_test(is_admin)
def user_profile(request, user_id):
    """
    A view to see details of a specific user and edit his/her details

    Args:
        request: the http request
        user_id: the current user to be edited

    Returns:
        An http redirect to itself if editing user failed or
        an html with appropriate messages if necessary
    """

    selected_user = get_object_or_404(get_user_model(), pk=user_id)
    kwargs = {
        'is_wizard': is_wizard(request.user),
        'is_judge': is_judge(request.user)
    }

    finished_tasks = get_finished_tasks(request.user, **kwargs)
    expired_tasks = get_expired_tasks(request.user, **kwargs)
    unfinished_tasks = get_unfinished_tasks(request.user, **kwargs)
    success_tasks, unsuccess_tasks, total_tasks = get_success_tasks(request.user, **kwargs)

    success_tasks_percent = get_percent(success_tasks, total_tasks)
    if kwargs['is_wizard']:
        finished_tasks_percent = get_percent(finished_tasks, request.user.wizard_tasks.count())
        expired_tasks_percent = get_percent(expired_tasks, request.user.wizard_tasks.count())
        # active_tasks_percent = get_percent(unfinished_tasks, Task.objects.filter(wizardassignment__state__lte=Task.UNFINISHED).count())
    elif kwargs['is_judge']:
        finished_tasks_percent = get_percent(finished_tasks, request.user.judge_tasks.count())
        expired_tasks_percent = get_percent(expired_tasks, request.user.judge_tasks.count())
        # active_tasks_percent = get_percent(unfinished_tasks, Task.objects.filter(judgeassignment__state__lte=Task.UNFINISHED).count())
    else:
        return HttpResponseBadRequest()

    context = {
        "selected_user": selected_user,
        "finished_tasks": finished_tasks,
        "finished_tasks_percent": finished_tasks_percent,
        "success_tasks": success_tasks,
        "success_tasks_percent": success_tasks_percent,
        "expired_tasks": expired_tasks,
        "expired_tasks_percent": expired_tasks_percent,
        "unfinished_tasks": unfinished_tasks,
        # "active_tasks_percent": active_tasks_percent,
    }

    # process logic for editing user and profile
    response = edit_user_logic(request, selected_user)

    # if the response is http redirect, redirect
    # else the response is context variables for template
    if isinstance(response, HttpResponseRedirect):
        return response
    else:
        context.update(response)

    return render(request, "core/user_profile.html", context)


def get_percent(numerator, denominator):
    """
    Get percentage based on numerator and denominator
    and evaluate to 0 if denominator is 0
    """

    if denominator == 0:
        return 0
    else:
        return numerator/denominator * 100