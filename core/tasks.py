import logging

from django.utils.html import strip_tags

from arv import celery_app
from celery import task
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template, render_to_string

from arv import settings
from core.utils import log_failure


@celery_app.task
def account_created(user_email, user_name, user_password, user_role, activation_link):
    """
    Sends an email to a new user to confirm that his/her email
    and inform them about user creation
    """


    # create context for the email template
    context = {
        "user_name": user_name,
        "user_password": user_password,
        "user_role": user_role,
        "activation_link": activation_link,
    }

    subject, from_email, to = 'Confirm your psi account', settings.EMAIL_HOST_USER, user_email

    html_content = render_to_string('core/email_templates/account_created.html', context)  # render with dynamic value
    text_content = strip_tags(html_content)  # Strip the html tag. So people can see the pure text at least.

    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    is_sent = msg.send()

    # # get the appropriate email template that will be populated with necessary data
    # email_template = get_template('core/email_templates/account_created.html')

    # content = email_template.render(context)
    #
    # # create an email and try to send it
    # email = EmailMessage("Confirm your psi account", content, settings.EMAIL_HOST_USER, [user_email])
    # email.content_subtype = "html"
    # is_sent = email.send()

    # log information needed to resend the email if email sending failed
    if not is_sent:
        # log information necessary to resend the email
        logger = logging.getLogger(__name__)

        logger.error(
            "Email sending for %s as %s about account creation has failed.",
            user_email, user_role
        )

        # insert all the necessary information to log the failure
        context["email"] = user_email
        context["user_password"] = user_password
        context["user_role"] = user_role
        context["activation_link"] = activation_link
        context["reason"] = "account_creation_email"

        # log failure to a file in a way that contains necessary information to recover
        log_failure(context)


@celery_app.task
def check_stocks(self):
    pass
