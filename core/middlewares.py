from django.utils import timezone
from django.contrib.auth.models import User

from .models import Profile


class TimezoneMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        # user cannot be anonymous user or other kind of user
        if isinstance(request.user, User):
            # if user has no profile info, give it a default one
            if not hasattr(request.user, "profile"):
                profile = Profile()
                profile.user = request.user
                profile.save()

            # set current timezone to user preferred timezone
            timezone.activate(request.user.profile.timezone)

        # get the response
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
