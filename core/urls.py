from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'core'
urlpatterns = [
    # url for login page
    url(r'^(?:accounts/login/)?$', auth_views.LoginView.as_view(template_name='core/login.html'), name='login'),

    # user is redirected here when he/she has successfuly logged in i.e "accounts/profile/"
    # then views.role_login will take it from there i.e. handling user types and the like
    url(r'^accounts/profile/$', views.role_login, name='role_login'),

    # url for logout request
    url(r'^accounts/logout/$', views.logout_user, name='logout'),

    # url to edit user's own profile
    url(r'^accounts/self_profile/$', views.self_profile, name="self_profile"),

    # url to confirm email and activate new user
    url(r'^accounts/confirm/activate/(?P<user_id>[0-9]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.confirm_and_activate, name="confirm_and_activate"),

    url(r'^dashboard/$', views.admin_dashboard, name="admin_dashboard"),

    # url to display  user detail
    url(r'^user_profile/(?P<user_id>\d+)/$', views.user_profile, name="user_profile"),
]