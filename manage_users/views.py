import pytz
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import Group, User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from core.forms import ProfileForm, UserForm
from core.tasks import account_created
from core.tokens import account_activation_token
from core.utils import is_admin, edit_user_logic, get_paginated_items, get_pages, ajax_required, \
    get_status


##############################################################################
# Views Related to Managing Users                                            #
##############################################################################


@user_passes_test(is_admin)
def view_users(request):
    """
    A view to show users and profile info
    """

    users_list = get_user_model().objects.exclude(is_superuser=True)

    users = get_paginated_items(request, users_list)
    pages = get_pages(users)

    # context variable for template
    context = {
        "users": users,
        "pages": pages,
    }

    return render(request, "manage_users/view_users.html", context)


@ajax_required
@user_passes_test(is_admin)
def get_filtered_users(request):
    """
    Return a table of users filtered by submitted criteria
    """

    # get the user attribute to be filtered
    attribute = request.GET.get("attribute")
    # get the search term
    search_term = request.GET.get("search_term").strip()

    if len(search_term) == 0:
        # we don't need filtering criteria
        kwargs = {}
    # if attribute is user attribute
    elif hasattr(request.user, attribute):
        # construct keyword args for field look up
        kwargs = {
            "{0}__{1}".format(attribute, "icontains"): search_term,
        }
    # if attribute is profilte attribute
    elif hasattr(request.user.profile, attribute):
        # construct keyword args for field look up
        kwargs = {
            "{0}__{1}__{2}".format("profile", attribute, "icontains"): search_term,
        }
    else:  # if attribute is user role
        # construct keyword args for field look up
        kwargs = {
            "{0}__{1}__{2}".format("groups", "name", "icontains"): search_term,
        }

    # get the account state to be filtered
    account_state = request.GET.get("account_state")
    if account_state == 'active':
        kwargs['is_active'] = True
    elif account_state == 'inactive':
        kwargs['is_active'] = False

    # get the account type to be filtered
    account_type = request.GET.get("account_type")
    if account_type == 'wizards' or account_type == 'judges':
        kwargs['groups__name'] = account_type

    # get users based on kwargs
    users_list = get_user_model().objects.filter(**kwargs).exclude(is_superuser=True)

    users = get_paginated_items(request, users_list)
    pages = get_pages(users)

    # context variable for template
    context = {
        "users": users,
        "pages": pages,
    }

    return render(request, "manage_users/ajax_responses/filtered_users_table.html", context)


@ajax_required
@user_passes_test(is_admin)
def deactivate_user(request):
    """
    A view to deactivate a user
    """

    user_id = int(request.POST['item_id'])
    user = get_object_or_404(get_user_model(), id=user_id)
    user.is_active = False
    user.save()

    return HttpResponse("Deactivated Successfully")


@ajax_required
@user_passes_test(is_admin)
def activate_deactivate_user(request):
    """
    A view to activate or deactivate a user
    """

    user_id = int(request.POST['user_id'])
    user = get_object_or_404(get_user_model(), id=user_id)

    # activate or deactivate based on request
    if request.POST.get("is_active") == "true":
        user.is_active = False
    else:
        user.is_active = True

    user.save()

    return HttpResponse("Processed Successfully")


@user_passes_test(is_admin)
def register_user(request):
    """
    A view to register new user
    """

    # if form is submitted
    if request.method == "POST":
        # populate both forms from submitted data
        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)

        # create new user from form and send an email to the new user if successful
        is_registered = create_user_from_form(request, user_form, profile_form)

        # save the success info if the creating process was successful
        request.session['registered'] = is_registered
        # save the failure info if the creating process was unsuccessful
        request.session['not_registered'] = not is_registered
        # save the errors that could arise from form submission
        request.session['user_form_errors'] = user_form.errors
        request.session['profile_form_errors'] = profile_form.errors

        # redirect to itself
        return redirect(request.path)
    else:  # if request method is GET
        # create new forms
        user_form = UserForm()
        profile_form = ProfileForm()

    # check if there was successful registration of user and invalidate it
    success = get_status(request, "registered", False)
    # check if there was unsuccessful registration of user and invalidate it
    failure = get_status(request, "not_registered", False)
    # get errors if there are any and invalidate them
    user_form_errors = get_status(request, "user_form_errors", {})
    profile_form_errors = get_status(request, "profile_form_errors", {})

    # context variables for template
    context = {
        "user_form": user_form,
        "profile_form": profile_form,
        "timezones": pytz.common_timezones,
        "success": success,
        "failure": failure,
        "user_form_errors": user_form_errors,
        "profile_form_errors": profile_form_errors,
    }

    return render(request, "manage_users/register_user.html", context)


# N.B This method is deprecated
@user_passes_test(is_admin)
def edit_user(request, user_id):
    """
    A view to edit a specific user and profile

    Args:
        request: the http request
        user_id: the current user to be edited

    Returns:
        An http redirect to itself if editing user failed or
        an html that illustrates the change made by editing the user
    """

    # get the user to be edited
    user = get_object_or_404(get_user_model(), id=int(user_id))

    # process logic for editing user and profile
    response = edit_user_logic(request, user)

    # if the response is http redirect, redirect
    # else the response is context variables for template
    if isinstance(response, HttpResponseRedirect):
        return response
    else:
        return render(request, "manage_users/edit_user.html", response)


def create_user_from_form(request, user_form, profile_form):
    """
    A logic to create a new user from form data and send an email to the new user
    to activate his/her account if successful

    Args:
        request: the http request
        user_form: user data populated UserForm instance
        profile_form: user data or default data populated ProfileForm instance

    Returns:
        A boolean indicating if the creation of the user is successful or not
    """

    # if forms are valid, update user with its profile
    if user_form.is_valid() and profile_form.is_valid():
        # create user from form
        new_user = user_form.save(commit=False)
        user_password = User.objects.make_random_password()
        new_user.set_password(user_password)
        new_user.is_active = False
        new_user.save()

        # create profile from form and associate the user with it
        new_profile = profile_form.save(commit=False)
        new_profile.user = new_user
        new_profile.save()

        # assign user to appropriate group
        new_user.groups.clear()
        group = Group.objects.get(name=user_form.cleaned_data['role'])
        group.user_set.add(new_user)

        # create an activation link for the user
        # user_id_b64 = urlsafe_base64_encode(force_bytes(new_user.pk))
        token = account_activation_token.make_token(new_user)
        relative_activation_url = reverse("core:confirm_and_activate", args=[str(new_user.id), token])
        activation_url = request.build_absolute_uri(relative_activation_url)

        # send an email to the new user to activate his/her account
        account_created.delay(new_user.email, new_user.username, user_password,
                              user_form.cleaned_data['role'], activation_url)

        return True

    # if one of the forms is invalid
    return False



