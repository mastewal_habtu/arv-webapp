from django.conf.urls import url

from . import views

app_name = 'manage_users'
urlpatterns = [
    # urls that are associated with managing users
    url(r'^view_users/$', views.view_users, name="view_users"),
    url(r'^register_user/$', views.register_user, name='register_user'),

    # deprecated view url
    # url(r'^edit_user/(?P<user_id>\d+)/$', views.edit_user, name="edit_user"),

    url(r'^get_filtered_users/$', views.get_filtered_users, name="get_filtered_users"),

    url(r'^activate_deactivate_user/$', views.activate_deactivate_user, name="activate_deactivate_user"),
]