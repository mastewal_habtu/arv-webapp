$(function () {
    /* 1. OPEN THE FILE EXPLORER WINDOW */
    $(".js-upload-photos").click(function () {
        $("#fileupload_input").click();

        $("#feedback_div").empty();
    });

    /* 2. INITIALIZE THE FILE UPLOAD COMPONENT */
    $("#fileupload_input").fileupload({
        dataType: 'json',
        sequentialUpload: true,

        start: function (e) {  /* 2. WHEN THE UPLOADING PROCESS STARTS, SHOW THE MODAL */
            $("#modal-progress").modal("show");
        },

        stop: function (e) {  /* 3. WHEN THE UPLOADING PROCESS FINALIZE, HIDE THE MODAL */
            $("#modal-progress").modal("hide");
        },

        progressall: function (e, data) {  /* 4. UPDATE THE PROGRESS BAR */
            var progress = parseInt(data.loaded / data.total * 100, 10);
            var strProgress = progress + "%";
            $(".progress-bar").css({"width": strProgress});
            $(".progress-bar").text(strProgress);

        },

        done: function (e, data) {  /* 3. PROCESS THE RESPONSE FROM THE SERVER */
            console.log("data.result: ", data.result);

            if (data.result.uploaded) {
                $("#feedback_div").append(
                    "<div class=\"alert alert-success alert-dismissible\">\n" +
                    "    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>\n" +
                    "    <h4>\n" +
                    "        <i class=\"icon fa fa-check\"></i>\n" +
                    "        Image " + data.result.name + " uploaded successfully" +
                    "    </h4>\n" +
                    "</div>"
                );
            } else {
                // Iterate through error messages sent from django image uploading view
                // Actually, the error sent in the json is the image uploading form error of django dictionary
                var error_messages = "";
                for (var field in data.result.errors) {
                    var array_len = data.result.errors[field].length
                    if (array_len > 0) {
                        for (var i = 0; i < array_len; i++) {
                            error_messages += data.result.errors[field][i] + "\n";
                        }
                    }
                }

                $("#feedback_div").append(
                    "<div class=\"alert alert-danger alert-dismissible\">\n" +
                    "    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>\n" +
                    "    <h4>\n" +
                    "        <i class=\"icon fa fa-warning\"></i>\n" +
                    "        Image " + data.result.name + " uploading failed!\n" +
                    "    </h4>\n" +
                    "    " + error_messages + "!\n" +
                    "</div>"
                );
            }
        }
    });

});