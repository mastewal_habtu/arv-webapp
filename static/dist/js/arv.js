/**
 * Created by yadda on 8/2/17.
 */

// ajax request to get filtered items
function get_filtered_items(search_term_id, attribute_id) {

    var data = {
        "search_term": $("#" + search_term_id).val(),
        "attribute": $("#" + attribute_id).val(),
        "account_type": $("#account_type").val(),
        "account_state": $("#account_state").val(),
    };

    // get the table id to be replaced
    var table_id = $("#" + search_term_id).attr("data-table-id");

    $.get($("#" + search_term_id).attr("data-ajax-url"), data)
        .done(function (response) {
            console.log(response);
            $("#" + table_id).html(response);
            assign_delete_item_handler();
            set_user_task_assign_remove_handler();
        })
        .fail(function () {
            console.log("Ajax just failed");
        });
}

// event handler if key is pressed to search something
$("#search_term").keyup(function (e) {
    get_filtered_items("search_term", "attribute");
});

// event handler if attribute selection is changed to search something
$("#attribute").change(function (e) {
    get_filtered_items("search_term", "attribute");
});

// event handler if attribute selection is changed to search something
$("#account_state").change(function (e) {
    get_filtered_items("search_term", "attribute");
});

// event handler if attribute selection is changed to search something
$("#account_type").change(function (e) {
    get_filtered_items("search_term", "attribute");
});

// event handler if key is pressed to search wizard
$("#wizard_search_term").keyup(function (e) {
    get_filtered_items("wizard_search_term", "wizard_attribute");
});

// event handler if key is pressed to search judge
$("#judge_search_term").keyup(function (e) {
    get_filtered_items("judge_search_term", "judge_attribute");
});

// event handler if attribute selection is changed to search wizard
$("#wizard_attribute").change(function (e) {
    get_filtered_items("wizard_search_term", "wizard_attribute");
});

// event handler if attribute selection is changed to search judge
$("#judge_attribute").change(function (e) {
    get_filtered_items("judge_search_term", "judge_attribute");
});

// function that assign delete item handler to links
function assign_delete_item_handler() {
    // remove click handler
    $(".delete_item").off("click");

    // assign click handler
    $(".delete_item").click(function (evt) {
        // prevent default functionality
        evt.preventDefault();

        var data = {
            "item_id": $(this).attr("data-id"),
            // "csrfmiddlewaretoken": $(this).attr("data-csrf-token"),
        };

        var parent = $(this).parent("td").parent("tr")
        var ajax_url = $(this).attr("data-ajax-url");

        bootbox.dialog({
            message: "Are you sure you want to Delete ?",
            title: "<i class='glyphicon glyphicon-trash'></i> Delete Item !",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success",
                    callback: function () {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: "Delete!",
                    className: "btn-danger",
                    callback: function () {
                        $.post(ajax_url, data)
                            .done(function (response) {
                                bootbox.alert(response);
                                parent.fadeOut('slow');
                            })
                            .fail(function () {
                                bootbox.alert('Something Went Wrong ....\n Please, try again.');
                            });
                    }
                }
            }
        });
    });
}

// assign delete item handler to delete links
assign_delete_item_handler();

// function that assign user assignment or removal on task handler to links
function set_user_task_assign_remove_handler() {
    // remove click handler
    $(".assign_remove_user").off("click");

    // assign click handler then
    $(".assign_remove_user").click(function (evt) {
        // prevent default functionality
        evt.preventDefault();

        // current selected button
        var btn = $(this);

        // get the user type of button
        var user_type = btn.attr("data-user-type");
        //set table element based on user type
        if (user_type == "wizard") {
            var table = $("#assigned_wizards");
        } else {
            var table = $("#assigned_judges");
        }

        // get ajax url
        var ajax_url = table.attr("data-ajax-url");

        // check if the action should be assigning or removing
        var is_assignment = btn.attr("data-assignment");

        // data to be submitted
        var data = {
            "user_id": btn.attr("data-id"),
            "assignment": is_assignment,
            "csrfmiddlewaretoken": table.attr("data-csrf-token"),
            "task_id": table.attr("data-task-id"),
        };

        // change the text to give feedback
        if (is_assignment == "true") {
            btn.text("Assigning...");
        } else {
            btn.text("Removing...");
        }

        // ajax request
        $.post(ajax_url, data)
            .done(function (response) {

                // reverse button state if button exist in first table
                var possible_btn1 = $("#assigned-user-" + btn.attr("data-id") + " button");
                reverseButtonState(possible_btn1, is_assignment);
                // reverse button state if button exist in second table
                var possible_btn2 = $("#user-" + btn.attr("data-id") + " button");
                reverseButtonState(possible_btn2, is_assignment);

                if (is_assignment == "true") {
                    // insert it in to assigned table
                    var tr = $("#user-" + btn.attr("data-id"));
                    var new_tr = tr.clone();
                    new_tr.attr("id", "assigned-user-" + btn.attr("data-id"));
                    table.append(new_tr);

                    // set action handler
                    set_user_task_assign_remove_handler();

                    // hide noone assigned message
                    if (user_type == "wizard") {
                        $("#no-wizard-assigned").hide();
                    } else {
                        $("#no-judge-assigned").hide();

                    }

                } else {
                    // remove it from assigned table
                    $("#assigned-user-" + btn.attr("data-id")).remove();
                }

                console.log("Ajax was successful");
            })
            .fail(function () {
                // set the text content as it was before
                if (is_assignment == "true") {
                    btn.text("Assign");
                } else {
                    btn.text("Remove");
                }

                bootbox.alert('Something Went Wrong ....\n Please, try again.');

            });

    });
}

// set handler that assign and remove user with task
set_user_task_assign_remove_handler();

// toggle button state betweend "Assign" and "Remove"
function reverseButtonState(btn, is_assignment) {
    if (is_assignment == "true") {
        // change the button to remove button
        btn.removeClass("btn-success");
        btn.addClass("btn-danger");
        btn.text("Remove");
        btn.attr("data-assignment", "false");
    } else {
        // change the button to assign button
        btn.removeClass("btn-danger");
        btn.addClass("btn-success");
        btn.text("Assign");
        btn.attr("data-assignment", "true");
    }
}


// function that assign activatation or deactivation of user handler to links
function set_user_activation_deactivation_handler() {
    // remove click handler
    $(".activate_deactivate_user").off("click");

    // assign click handler then
    $(".activate_deactivate_user").click(function (evt) {
        // prevent default functionality
        evt.preventDefault();

        // current selected button
        var btn = $(this);

        // get ajax url
        var ajax_url = btn.attr("data-ajax-url");

        // check if the action should be activating or deactivating
        var is_active = btn.attr("data-user-active");

        // data to be submitted
        var data = {
            "user_id": btn.attr("data-id"),
            "is_active": is_active,
        };



        // verb to be used in messages which could be "activate" or "deactivate"
        var verb = (is_active == "true")? "Deactivate" : "Activate";
        bootbox.dialog({
            message: "Are you sure you want to " + verb + " user?",
            title: "<i class='glyphicon glyphicon-trash'></i> " + verb + " user!",
            buttons: {
                success: {
                    label: "No",
                    className: "btn-success",
                    callback: function () {
                        $('.bootbox').modal('hide');
                    }
                },
                danger: {
                    label: verb + "!",
                    className: "btn-danger",
                    callback: function () {
                        // change the text to give feedback
                        if (is_active == "true") {
                            btn.text("Deactivating...");
                        } else {
                            btn.text("Activating...");
                        }

                        // ajax request
                        $.post(ajax_url, data)
                            .done(function (response) {

                                // reverse button state
                                reverseUserActivationButtonState(btn, is_active);

                                console.log("Ajax was successful");
                            })
                            .fail(function () {
                                // set the text content as it was before
                                if (is_active == "true") {
                                    btn.text("Deactivate Account");
                                } else {
                                    btn.text("Active Account");
                                }

                                bootbox.alert('Something Went Wrong ....\n Please, try again.');

                            });
                    }
                }
            }
        });

    });
}

// set handler that activate and deactivate a user
set_user_activation_deactivation_handler();

// toggle button state betweend "Activate" and "Deactivate"
function reverseUserActivationButtonState(btn, is_active) {
    if (is_active == "true") {
        // change the button to activate button
        btn.removeClass("btn-danger");
        btn.addClass("btn-success");
        btn.text("Activate Account");
        btn.attr("data-user-active", "false");
    } else {
        // change the button to deactivate button
        btn.removeClass("btn-success");
        btn.addClass("btn-danger");
        btn.text("Deactivate Account");
        btn.attr("data-user-active", "true");
    }
}

// preview selected image in the same page
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            // hide image status
            $("#image_status").hide();
            // create the image tag
            var img = $("<img>").attr('src', e.target.result)
                .attr("width", "100%")
                .attr("id", "image_preview")
                .slideDown("slow");
            // insert image tag into DOM
            $("#image_status").after(img);
        };

        reader.readAsDataURL(input.files[0]);
    } else {
        // show image status
        $("#image_status").show();
        // remove the image preview tag
        $("#image_preview").remove();
    }
}

// assign handler when image selection is changed
$("#image_to_upload").change(function () {
    readURL(this);
});

// assign behaviours for datetime pickers as necessary
$('#sketch_deadline').datetimepicker({
    minDate: moment(),
});
$('#judgment_deadline').datetimepicker({
    minDate: moment(),
});
$('#trade_start').datetimepicker({
    minDate: moment(),
});
$('#trade_exit').datetimepicker({
    minDate: moment(),
});
$('#notification').datetimepicker({
    minDate: moment(),
});


// make select control a timezone picker
// $('#timezone').timezones();


// function that assign upload image handler to tasks
function assign_upload_image_handler() {
    // remove change handler
    $(".upload_image").off("change");

    // assign change handler
    $(".upload_image").change(function (evt) {
        // prevent default functionality
        evt.preventDefault();

        var formdata = new FormData();
        var hidden_input = $("#hidden_info");
        var btn = $(this);

        var data = {
            "task_id": $(this).attr("data-task-id"),
            "wizard_id": hidden_input.attr("data-wizard-id"),
            "csrfmiddlewaretoken": hidden_input.attr("data-csrf-token"),
        };

        var ajax_url = hidden_input.attr("data-ajax-url");

        var file = this.files[0];
        if (formdata) {
            formdata.append("image", file);
            formdata.append("task_id", $(this).attr("data-task-id"));
            formdata.append("wizard_id", hidden_input.attr("data-wizard-id"));
            formdata.append("csrfmiddlewaretoken", hidden_input.attr("data-csrf-token"));
        }

        $.ajax({
            url: ajax_url,
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (response) {
                bootbox.alert(response);
                $("#" + btn.attr("data-task-id")).remove();     // remove that task
                console.log("Ajax successful");
            },
            error: function () {
                bootbox.alert('Something Went Wrong ....\n Please, try again.');
                console.log("Ajax failed");
            }
        });
    });
}

// assign upload image handler
assign_upload_image_handler();


// assign voted image viewer
function assign_voted_image_viewer() {
    $(".voted_image_viewer").click(function (evt) {
        evt.preventDefault();

        voted_src = $(this).attr("data-voted-image-url");
        event_src = $(this).attr("data-event-image-url");
        voted_img = "<div class='col-md-12'>" +
            "<h4 class='text-center'>System Voted Image</h4>" +
            "<img src='" + voted_src + "' style='max-width:300px;'>" +
            "</div>";
        event_img = "<div class='col-md-12'>" +
            "<h4 class='text-center'>Real Happened Image</h4>" +
            "<img src='" + event_src + "' style='max-width:300px;'>" +
            "</div>";
        bootbox.alert(voted_img + event_img);
    });
}

assign_voted_image_viewer();


// assign image feedback viewer
function assign_feedback_image_viewer() {
    $(".feedback_image_viewer").click(function (evt) {
        evt.preventDefault();

        uploaded_src = $(this).attr("data-predicted-image-url");
        feedback_src = $(this).attr("data-event-image-url");
        uploaded_img = "<div class='col-md-12'>" +
            "<h4 class='text-center'>Uploaded Image</h4>" +
            "<img src='" + uploaded_src + "' style='max-width:300px;'>" +
            "</div>";
        feedback_img = "<div class='col-md-12'>" +
            "<h4 class='text-center'>Feedback Image</h4>" +
            "<img src='" + feedback_src + "' style='max-width:300px;'>" +
            "</div>";
        bootbox.alert(uploaded_img + feedback_img);
    });
}

assign_feedback_image_viewer();


// get random image handler
$("#get_random_image").click(function (evt) {
    evt.preventDefault();

    var btn = $(this);
    // change the text of the button to give promise
    btn.text("Getting Image ...");

    ajax_url = btn.attr("data-ajax-url");

    $.ajax({
        url: ajax_url,
        success: function (response) {
            var random_src = response;
            var heading = "<h4 class='text-center'>Random Image</h4>";
            var random_img =
                "<img src='" + random_src + "' style='max-width:300px;'>"
                ;
            bootbox.alert({
                size: "normal",
                title: heading,
                message: random_img,
                backdrop: true,
            });
            // change button text to original text
            btn.text("Show Random Image");
            console.log("Ajax successful");
        },
        error: function () {
            bootbox.alert('Something Went Wrong ....\n Please, try again.');
            // change button text to original text
            btn.text("Show Random Image");
            console.log("Ajax failed");
        },
    });
});


// vote image as a judge
$(".submitter").click(function (evt) {
    evt.preventDefault();

    var hidden_input = $("#hidden_info");
    var btn = $(this);

    btn.text("Submitting...");
    var wizard_id = btn.attr("data-wizard-id");
    var image_1 = $('input[data-which-image='+wizard_id+'_1]:checked').val();
    var image_2 = $('input[data-which-image='+wizard_id+'_2]:checked').val();
    if (image_1 === undefined || image_2 === undefined){
        bootbox.alert("Please, Give a rank point for both images before submitting.");
        btn.text("Submit Ranks");
        return;
    }
    console.log(image_2);

    var data = {
        "task_id": hidden_input.attr("data-task-id"),
        "wizard_id": wizard_id,
        "image_1": image_1,
        "image_2": image_2,
        "csrfmiddlewaretoken": hidden_input.attr("data-csrf-token"),
    };

    var parent = $("#" + data.wizard_id);

    var ajax_url = hidden_input.attr("data-ajax-url");

    $.ajax({
        url: ajax_url,
        type: "POST",
        data: data,
        success: function (response) {
            // set_btns_state(parent, data.which_image);
            btn.text("Ranks submitted");
            console.log("Ajax was successful");
        },
        error: function () {
            // set button text to its original text
            btn.text("Submit Ranks");
            bootbox.alert("Something Went Wrong ....\n Please, try again.")
        },
    });
});


// set the buttons of a parent to voted or not
function set_btns_state(parent, which_image) {
    btns = parent.find("button");
    btns.each(function (index) {
       if ($(this).attr("data-which-image") == which_image){
           $(this).html("<i class='fa fa-check'></i>  Ranks Submitted");
       } else{
           $(this).html("Submit Ranks");
       }
    });
}


