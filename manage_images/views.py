from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.template import loader

from manage_images.forms import ImageUploadForm, PotentialImageUpdateForm
from manage_images.models import Image

from core.utils import get_status, get_paginated_items, get_pages, ajax_required, is_admin


@ajax_required
@user_passes_test(is_admin)
def add_images(request):
    """
    A view to add/upload multiple images asynchronously i.e. the AJAX way
    """

    if request.method == "POST":
        image_form = ImageUploadForm(request.POST, request.FILES)

        data = {"name": request.FILES["image"].name}

        if image_form.is_valid():
            image_form.save()

            data['uploaded'] = True

        else:
            data['uploaded'] = False

            data['errors'] = image_form.errors

        return JsonResponse(data)


# N.B This view is deprecated and not used
@user_passes_test(is_admin)
def add_image(request):
    """
    A view to add/upload new single task image
    """

    if request.method == "POST":
        image_form = ImageUploadForm(request.POST, request.FILES)

        if image_form.is_valid():
            image_form.save()

            # save the success info if the uploading process was successful
            request.session['uploaded'] = True
        else:
            # save the failure info if the uploading process was unsuccessful
            request.session['not_uploaded'] = True

            # save the errors that could arise from form submission
            request.session['image_form_errors'] = image_form.errors

        return redirect(request.path)

    else:
        image_form = ImageUploadForm()

    # check if there was successful upload of image and invalidate it
    success = get_status(request, 'uploaded', False)
    # check if there was unsuccessful upload of image and invalidate it
    failure = get_status(request, 'not_uploaded', False)
    # get errors if there are any and invalidate them
    image_form_errors = get_status(request, 'image_form_errors', {})

    # context variables for template
    context = {
        "image_form": image_form,
        "success": success,
        "failure": failure,
        "image_form_errors": image_form_errors,
    }

    return render(request, "manage_images/add_images.html", context)


@user_passes_test(is_admin)
def view_images(request):
    """
    A view to show all task images
    """

    images_list = Image.objects.all()

    # get images on current page
    images = get_paginated_items(request, images_list)

    # get page number range
    pages, many_pages = get_pages(images)

    # context variables for template
    context = {
        'images': images,
        'pages': pages,
        'many_pages': many_pages,
        'all_images': images_list,
    }

    return render(request, "manage_images/view_images.html", context)


@ajax_required
@user_passes_test(is_admin)
def get_filtered_images(request):
    """
    Return a table of images filtered by submitted criteria
    """

    # get the user attribute to be filtered
    attribute = request.GET.get("attribute")
    # get the search term
    search_term = request.GET.get("search_term").strip()

    if len(search_term) == 0:
        # we don't need filtering criteria
        kwargs = {}

    elif attribute == "id":
        try:
            int(search_term)

            # construct keyword args for field look up if number is integer
            kwargs = {
                "{0}__{1}".format(attribute, "exact"): search_term,
            }
        except ValueError:
            # construct keyword args for field look up if number is not integer
            kwargs = {
                "{0}__{1}".format(attribute, "exact"): 0,
            }

    else:
        # construct keyword args for field look up
        kwargs = {
            "{0}__{1}".format(attribute, "icontains"): search_term,
        }

    images_list = Image.objects.filter(**kwargs)

    # get images on current page
    images = get_paginated_items(request, images_list)

    # get page number range
    pages, many_pages = get_pages(images)

    context = {
        "images": images,
        "pages": pages,
        "many_pages": many_pages,
    }

    return render(request, "manage_images/ajax_responses/filtered_images_table.html", context)


@ajax_required
@user_passes_test(is_admin)
def delete_image(request):
    """
    Deletes a task image (N.B. Not used feature)

    Actually, an image must be checked if it is in active use for a task or tasks
    which will definitely lead to abnormal behavior and useless scenario, consequently
    this feature is dropped for now because no check up is made.
    """

    image_id = int(request.POST['item_id'])
    image = get_object_or_404(Image, pk=image_id)

    # image.delete()

    return HttpResponse("You can not delete any image.Any image may be in active use.")


@user_passes_test(is_admin)
def update_potential_images(request, image_id):
    """
    Updates potential images for a specific image

    Redirects to view_images page
    """

    image = get_object_or_404(Image, pk=image_id)
    potential_images_form = PotentialImageUpdateForm(request.POST, instance=image)

    if potential_images_form.is_valid():
        potential_images_form.save()

    return redirect("manage_images:view_images")