from django.conf.urls import url

from manage_images import views as image_views

app_name = 'manage_images'

urlpatterns = [
    # urls that are associated with managing images
    url(r'^add_image/$', image_views.add_image, name='add_image'), # Actually, this one is deprecated and not used
    url(r'^add_images/$', image_views.add_images, name='add_images'),
    url(r'^view_images/$', image_views.view_images, name="view_images"),
    url(r'^get_filtered_images/$', image_views.get_filtered_images, name="get_filtered_images"),
    url(r'^delete_image/$', image_views.delete_image, name="delete_image"),
    url(r'^update_potential_images/(?P<image_id>\d+)/$', image_views.update_potential_images, name="update_potential_images"),
]

