from django.forms import ModelForm, forms

from .models import Image


class ImageUploadForm(ModelForm):
    """
    A model form to upload an image which will be used with task
    """

    def __init__(self, *args, **kwargs):
        super(ImageUploadForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    def clean_image(self):
        """
        Checks the validity of the image name
        i.e. An image name should be a unique number
        """
        image = self.cleaned_data["image"]


        dot_index = image.name.rfind(".")
        if dot_index == -1:
            raise forms.ValidationError(
                "The name of the image file should include extension", code="invalid_number"
            )

        image_name = image.name[:dot_index]
        try:
            image_id = int(image_name)
            if Image.objects.filter(id__exact=image_id).exists():
                raise forms.ValidationError(
                    "The id {} is already used by another image".format(image_id),
                    code="invalid_id"
                )
            else:
                return image
        except ValueError:
            raise forms.ValidationError(
                "The name of the image file must be only a number", code="invalid_id"
            )

    def save(self, force_insert=False, force_update=False, commit=True):
        """
        Assumes:
            The filename of the image is a valid number

        Saves the model using the filename as number field for the model
        """

        image = super(ImageUploadForm, self).save(commit=False)

        # assume the name of the image is clean
        # i.e. the name of the image is an acceptable integer
        dot_index = image.image.name.rfind(".")
        image.id = int(image.image.name[:dot_index])

        if commit:
            image.save()

        return image


    class Meta:
        model = Image
        fields = ('image', 'caption', 'description',)


class PotentialImageUpdateForm(ModelForm):
    """
    A model form to update an image's potential images which will be used for task creation
    """

    def __init__(self, *args, **kwargs):
        super(PotentialImageUpdateForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)


    class Meta:
        model = Image
        fields = ('potentials',)

    def save(self, force_insert=False, force_update=False, commit=True):
        """
        Add or remove potential images accordingly, ignore adding itself as potential
        """

        image = super(PotentialImageUpdateForm, self).save(commit=False)

        image.potentials.clear()
        for potential in self.cleaned_data['potentials'].all():
            if image.id != potential.id:
                image.potentials.add(potential)

        if commit:
            image.save()

        return image