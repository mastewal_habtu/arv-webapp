from django.apps import AppConfig


class ManageImagesConfig(AppConfig):
    name = 'manage_images'

    def ready(self):
        import manage_images.signals