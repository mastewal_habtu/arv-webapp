from django.db import models

from arv import settings


class Image(models.Model):
    """
    Model Image to represent image that will be used to create task
    """

    image = models.ImageField(unique=True, upload_to=settings.TASK_IMAGES_FOLDER)
    caption = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=150, blank=True)
    upload_date = models.DateTimeField(auto_now_add=True)

    potentials = models.ManyToManyField("self")

    def __str__(self):
        return self.caption or self.description or super().__str__()

    class Meta:
        ordering = ["id"]


# Model to persist tha recent last used images in order to avoid repeating of images
class RecentImage(models.Model):
    image = models.OneToOneField(Image, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.image)
