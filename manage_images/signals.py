from django.db.models.signals import post_delete
from django.dispatch import receiver

from manage_images.models import Image


@receiver(post_delete, sender=Image)
def delete_task_image(sender, instance, *args, **kwargs):
    """
    A subscriber to delete an image file from filesystem when an image model
    is deleted in the database. Even if it is bulk delete or single model delete

    Args:
        sender: sender of the signal
        instance: the model instance that is deleted,
                  note that this instance is already deleted from database since signal is post_delete
    """

    instance.image.storage.delete(instance.image.path)

    print("Info: ", instance.image.path, " is deleted.")
