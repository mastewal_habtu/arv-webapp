from django.conf.urls import url
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from cue import views as cue_views

app_name = 'cue'
urlpatterns = [
    # urls that are associated with managing images
    url(r'^viewer_email/$', cue_views.viewer_email, name='viewer_email'),
    url(r'^judge_email/$', cue_views.judge_email, name='judge_email'),
    url(r'^feedback_email/$', cue_views.feedback_email, name='feedback_email'),
 
]

static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
