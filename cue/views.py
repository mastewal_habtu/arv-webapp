from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import get_template

import os
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from email.mime.image import MIMEImage
from django.conf import settings

from .forms import ViewermessageForm
from django.shortcuts import render, reverse, redirect, get_object_or_404


#notify_email
def viewer_email(request):

    form_class = ViewermessageForm

    if request.method == 'POST':
        form = form_class(data=request.POST)
        
        if form.is_valid():
            user = User.objects.get(id=1)
            contact_email = user.email
            
            Sketch_Deadline = 'Saturday, October 28, 2017 (GMT+3)' #request.POST.get('content', '')
            
            template = get_template('cue/viewer_email_template.txt')

            context = {
                        'Sketch_Deadline': Sketch_Deadline,
                    }

            content = template.render(context)

            email = EmailMessage("New ARV Tasking testing",
                                 content,
                                 "ARV Admin",
                                 ['tsegshi2003@gmail.com','tsega_end@yahoo.com'],
                                 headers = {'Reply-To': contact_email }
                            )
            
            email.content_subtype = "html"                     
            email.send()
            #return redirect('contact')
            return redirect(request.path)
        
    return render(request, 'cue/viewer_message.html', {'form': form_class,})
   
    
def judge_email(request):
    form_class = ViewermessageForm

    if request.method == 'POST':
        form = form_class(data=request.POST)
        
        if form.is_valid():
            user = User.objects.get(id=1)
            contact_email = user.email
            
            #contact_email = User.objects.get(username=request.user.email)
            Judges_Deadline = 'Saturday, October 28, 2017 (GMT+3)' #request.POST.get('content', '')
            
            template = get_template('cue/judge_email_template.txt')

            context = {
                        'Judges_Deadline': Judges_Deadline,
                    }

            content = template.render(context)
           
            email = EmailMessage("New ARV Task",
                                 content,
                                 "ARV Admin",
                                 ['tsegshi2003@gmail.com','tsega_end@yahoo.com'],
                                 headers = {'Reply-To': contact_email }
                            )
            
            email.content_subtype = "html"                     
            email.send()
            #return redirect('contact')
            return redirect(request.path)
        
    return render(request, 'cue/viewer_message.html', {'form': form_class,})

def feedback_email(request):
    form_class = ViewermessageForm

    if request.method == 'POST':
        form = form_class(data=request.POST)
        
        if form.is_valid():
            user = User.objects.get(id=1)
            contact_email = user.email
            
            #contact_email = User.objects.get(username=request.user.email)
            Feedback_Time = 'Saturday, October 28, 2017 (GMT+3)' #request.POST.get('content', '')
            
            template = get_template('cue/feedback_email_template.txt')

            context = {
                        'Feedback_Time': Feedback_Time,
                    }

            content = template.render(context)

            email = EmailMultiAlternatives("New ARV Feedback",
                     content,
                     "ARV Admin",
                     ['tsegshi2003@gmail.com','tsega_end@yahoo.com'],
                     headers = {'Reply-To': contact_email }
                )
            
            #email.attach_alternative(html_content, "text/html")
            #email.mixed_subtype = 'related'
            email.content_subtype = "html"
            
            #for filename, file_id in images:
            image_file = open(settings.MEDIA_ROOT + "/task_images/"+"0e8b7a76-893c-4dd3-8e42-cc34f5ec3c1e.jpg", 'rb')
            msg_image = MIMEImage(image_file.read())
            image_file.close()
            msg_image.add_header('Content-ID', '<%s>' % '0e8b7a76-893c-4dd3-8e42-cc34f5ec3c1e.jpg')
            email.attach(msg_image)
            
            email.send()

            return redirect(request.path)
        
    return render(request, 'cue/viewer_message.html', {'form': form_class,})
