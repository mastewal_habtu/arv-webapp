#!/usr/bin/env bash

# start redis server
redis-server

# check redis server


# start celery workers
celery -A arv worker -l info

# check celery workers

# start celery beat if needed
