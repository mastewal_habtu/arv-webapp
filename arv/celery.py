import os
from celery import Celery
from django.conf import settings

"""
A configuration setup for a Celery instance
"""

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'arv.settings')

app = Celery('arv', broker=settings.BROKER_URL)

app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda : settings.INSTALLED_APPS + settings.INSTALLED_APPS_WITHOUT_APPCONFIGS)