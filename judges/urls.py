from django.conf.urls import url

from . import views

app_name = 'judges'
urlpatterns = [
    url(r'^unfinished_tasks/$', views.view_unfinished_tasks, name="unfinished_tasks"),
    url(r'^unfinished_task_judgments/(?P<task_id>[0-9]+)$', views.view_unfinished_task_judgments,
        name="unfinished_task_judgments"),
    url(r'^submit_ranks/$', views.submit_ranks, name="vote_image"),
    url(r'^finished_tasks/$', views.view_finished_tasks, name="finished_tasks"),
    url(r'^expired_tasks/$', views.view_expired_tasks, name="expired_tasks"),

    # dummy url, doesn't work
    url(r'^solo_practice/$', views.view_expired_tasks, name="solo_practice"),
]