from django.shortcuts import render, redirect
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import get_object_or_404
from django.http.response import Http404, HttpResponseBadRequest, HttpResponse

from core.utils import get_paginated_items, get_user_profile, ajax_required
from manage_tasks.models import Task, JudgeAssignment, WizardAssignment

from judges.models import Judgment

# dictionary to help map sent request parameter to comphrensible constants
possible_images = {
    "no_event": Task.NO_EVENT,
    "image_1": Task.IMAGE_1,
    "image_2": Task.IMAGE_2,
}


def is_judge(user):
    """
        Check if user is judge and also the user has logged in

        Args:
              user: the current user in the http request

        Returns:
              A boolean indicating if user is logged in and is an judge
        """

    if user.is_authenticated:
        return user.groups.filter(name='judges').exists()
    else:
        return False


# view to show unfinished tasks
@user_passes_test(is_judge)
def view_unfinished_tasks(request):
    # get tasks that are unfinished and
    # order them based on whether they are new or not
    tasks_list = request.user.judge_tasks \
        .filter(judgeassignment__state__lt=Task.FINISHED) \
        .order_by('judgeassignment__state')
    # check if tasks have a wizard to be evaluated
    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    # change the states of new task to unfinished
    assignment_states = []
    for task in tasks:
        judge_assignment = get_object_or_404(JudgeAssignment, judge=request.user, task=task)
        assignment_states.append(judge_assignment.state)
        if judge_assignment.state == Task.ASSIGNED_NEW:
            judge_assignment.state = Task.UNFINISHED
            judge_assignment.save()

    # zip tasks with their corresponding judge assignment
    zipped_tasks = zip(tasks, assignment_states)

    # context variable for template
    context = {
        "zipped_tasks": zipped_tasks,
        "tasks": tasks,

        # constant
        "ASSIGNED_NEW": Task.ASSIGNED_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "judges/view_unfinished_tasks.html", context)


# view to show unfinished tasks
@user_passes_test(is_judge)
def view_unfinished_task_judgments(request, task_id):
    # get queryset of tasks assigned to judge
    tasks_list = request.user.judge_tasks.filter(judgeassignment__state__lt=Task.FINISHED)
    # get the requested task, if not found respond with "404 not found"
    try:
        task = tasks_list.get(pk=task_id)
    except Task.DoesNotExist:
        return redirect("judges:unfinished_tasks")

    # get the wizards who finished and not feededback assigned to the selected task
    # (actually get their relationship model wizard_assignment)
    assignments_list = task.wizardassignment_set.filter(state__lte=Task.FINISHED)

    # get them in paginated form
    assignments = get_paginated_items(request, assignments_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    # while collecting judgments for template referring,
    # checkout for necessary state updates
    judgments = []
    are_all_finished = True
    for assignment in assignments:
        # unfortunately, create judgment if it doesn't exist, weird right??
        judgment, is_created = Judgment.objects.get_or_create(wizardassignment=assignment, judge=request.user)

        judgments.append(judgment)
        # change the states of new judgments to unfinished
        if judgment.state == Task.ASSIGNED_NEW:
            judgment.state = Task.UNFINISHED
            judgment.save()

        # check if judging the task is not finished
        if judgment.state < Task.FINISHED:
            are_all_finished = False

    # if all wizard assignments are voted, consider the task as finished
    if are_all_finished:
        judge_assignment = task.judgeassignment_set.get(judge=request.user)
        judge_assignment.state = Task.FINISHED
        judge_assignment.save()

        # increase the number of tasks that a user has finished
        user_profile = get_user_profile(request.user)
        user_profile.finished_tasks += 1
        user_profile.save()

    # zip judgments with their assignments
    assignment_judgments = zip(assignments, judgments)

    # context variable for template
    context = {
        "task": task,
        "assignment_judgments": assignment_judgments,

        # constants
        "ASSIGNED_NEW": Task.ASSIGNED_NEW,
        "FINISHED": Task.FINISHED,
        "IMAGE_1": Task.IMAGE_1,
        "IMAGE_2": Task.IMAGE_2,
        "rank_scores": [0,1,2,3,4,5,6,7],
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "judges/view_unfinished_task_judgments.html", context)


# vote an image of task from the pair given as a judge
@ajax_required
@user_passes_test(is_judge)
def submit_ranks(request):
    # respond if request is post
    if not request.method == "POST":
        return HttpResponseBadRequest("Request method is not post.")

    # get necessary parameters to get wizard_assignment on a task
    task_id = int(request.POST["task_id"])
    wizard_id = int(request.POST["wizard_id"])
    wizard_assignment = get_object_or_404(WizardAssignment, task=task_id, wizard=wizard_id)

    # get image ranks
    rank_1 = int(request.POST["image_1"])
    rank_2 = int(request.POST["image_2"])

    # get the specific relation for the judgment of the wizard on selected task
    judgment = get_object_or_404(Judgment, wizardassignment=wizard_assignment, judge=request.user)

    judgment.rank_1 = rank_1
    judgment.rank_2 = rank_2
    # if judgment is not considered finished weirdly, make it happen
    if judgment.state < Task.FINISHED:
        judgment.state = Task.FINISHED
        judgment.save()
    return HttpResponse('Submitted successfully')


# view to show finished tasks
@user_passes_test(is_judge)
def view_finished_tasks(request):
    # get tasks that are finished and
    # order them based on whether they have new feedback or not
    tasks_list = request.user.judge_tasks \
        .filter(judgeassignment__state__gte=Task.FINISHED) \
        .exclude(judgeassignment__state__gte=Task.EXPIRED_NEW) \
        .order_by('judgeassignment__state')

    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    # change the states of has new feedback
    assignment = []
    for task in tasks:
        judge_assignment = get_object_or_404(JudgeAssignment, judge=request.user, task=task)
        assignment.append(judge_assignment)
        if judge_assignment.state == Task.FEEDEDBACK_NEW:
            judge_assignment.state = Task.FEEDEDBACK
            judge_assignment.save()

    # zip tasks with their corresponding judge assignment states
    zipped_tasks = zip(tasks, assignment)

    # context variable for template
    context = {
        "zipped_tasks": zipped_tasks,
        "tasks": tasks,

        # constant
        "FEEDEDBACK_NEW": Task.FEEDEDBACK_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "judges/view_finished_tasks.html", context)


# view to show expired tasks
@user_passes_test(is_judge)
def view_expired_tasks(request):
    # get tasks that are expired and
    # order them based on whether they are new or not
    tasks_list = request.user.judge_tasks \
        .filter(judgeassignment__state__gte=Task.EXPIRED_NEW) \
        .order_by("judgeassignment__state")

    # get the tasks in paginated form
    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    # change the states of new expired tasks
    assignment_states = []
    for task in tasks:
        judge_assignment = get_object_or_404(JudgeAssignment, judge=request.user, task=task)
        assignment_states.append(judge_assignment)
        if judge_assignment.state == Task.EXPIRED_NEW:
            judge_assignment.state = Task.EXPIRED
            judge_assignment.save()

    # zip tasks with their corresponding judge assignment_states
    zipped_tasks = zip(tasks, assignment_states)

    # context variable for template
    context = {
        "zipped_tasks": zipped_tasks,
        "tasks": tasks,

        # constant
        "EXPIRED_NEW": Task.EXPIRED_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "judges/view_expired_tasks.html", context)


# get the number of new tasks assigned and new feedbacks to finished tasks
def get_new_info_count(judge):
    # unfinished tasks to a user
    unfinished_tasks = judge.judge_tasks.filter(judgeassignment__state__lt=Task.FINISHED)
    unfinished_tasks_count = unfinished_tasks.count()

    # new assigned tasks to a user
    new_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.ASSIGNED_NEW)
    new_tasks_count = new_tasks.count()

    # new feededback tasks to a user
    new_feededback_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.FEEDEDBACK_NEW)
    new_feededback_tasks_count = new_feededback_tasks.count()

    # new expired tasks to a user
    new_expired_tasks = judge.judge_tasks.filter(judgeassignment__state=Task.EXPIRED_NEW)
    new_expired_tasks_count = new_expired_tasks.count()

    # context that contains count info to be used in template
    context = {
        "unfinished_tasks_count": unfinished_tasks_count,
        "new_tasks_count": new_tasks_count,
        "new_feededback_tasks_count": new_feededback_tasks_count,
        "new_expired_tasks_count": new_expired_tasks_count,
    }

    return context
