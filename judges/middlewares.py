from django.contrib.auth.models import User
from django.utils import timezone
from django.shortcuts import get_object_or_404

from core.utils import get_user_profile
from .views import is_judge
from manage_tasks.models import JudgeAssignment, Task

# change task to expired state if they passed their due times
class ExpiredTasksMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        # user cannot be anonymous user or other kind of user
        if isinstance(request.user, User):
            # user must be a wizard
            if is_judge(request.user):
                user_profile = get_user_profile(request.user)

                # get expired tasks for the user
                new_expired_tasks = request.user.judge_tasks\
                    .filter(judgment_deadline__lt=timezone.now())\
                    .exclude(judgeassignment__state=Task.EXPIRED)

                new_expired_tasks_count = 0
                # change states of the tasks to expired_new
                for task in new_expired_tasks:
                    judge_assignment = get_object_or_404(JudgeAssignment, task=task, judge=request.user)
                    judge_assignment.state = Task.EXPIRED_NEW
                    judge_assignment.save()

                    # increase the number of tasks expired for the user
                    new_expired_tasks_count += 1

                if new_expired_tasks_count > 0:
                    user_profile = get_user_profile(request.user)
                    user_profile.expired_tasks += new_expired_tasks_count
                    user_profile.save()

        # get the response
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response
