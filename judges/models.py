from django.db import models
from django.conf import settings

from manage_tasks.models import WizardAssignment, Task


class Judgment(models.Model):
    """
    A through table model for judge and wizard_assignment relationship
    """

    ranks = [
        (0, "0"),
        (1, "1"),
        (2, "2"),
        (3, "3"),
        (4, "4"),
        (5, "5"),
        (6, "6"),
        (7, "7"),
    ]

    judge = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    wizardassignment = models.ForeignKey(WizardAssignment, on_delete=models.CASCADE)
    state = models.SmallIntegerField(choices=Task.states, default=Task.ASSIGNED_NEW)

    # additional description associated to a specific relation
    rank_1 = models.SmallIntegerField(choices=ranks, blank=True, default=None, null=True)
    rank_2 = models.SmallIntegerField(choices=ranks, blank=True, default=None, null=True)

    rating = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.judge.username + " for " + str(self.wizardassignment)

    def is_correct(self):
        event_image = self.wizardassignment.task.event
        if self.rank_1 is None or self.rank_2 is None or event_image is None:
            return Task.NO_EVENT

        if self.rank_1 > self.rank_2:
            return event_image == Task.IMAGE_1
        elif self.rank_2 > self.rank_1:
            return event_image == Task.IMAGE_2

        return Task.NO_EVENT


    def voted_image(self):
        if self.rank_1 is None or self.rank_2 is None:
            return Task.NO_EVENT

        if self.rank_1 > self.rank_2:
            return Task.IMAGE_1
        elif self.rank_2 > self.rank_1:
            return Task.IMAGE_2

        return Task.NO_EVENT