from django.utils.html import strip_tags

from arv import celery_app
from celery import task
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template, render_to_string

import logging

from arv import settings
from core.utils import log_failure


@celery_app.task
def wizard_assignment(
        wizard_email, is_assignment, task_id, task_name,
        task_description, task_sketch_deadline, task_link):
    """
    Sends an email to a wizard to inform that he/she is
    assigned on a specific task asynchronously

    Args:
         wizard_email: the email of wizard that is recently assigned on a task
         is_assignment: whether the wizard is being assigned on a task or removed from
         task_id: the id of the task that a wizard is assigned on or removed from
         task_name: the name of the task
         task_description: the description of the task
         task_sketch_deadline: the sketch deadline of the task

    Returns:
        nothing, this function is asynchronous
    """

    # a format of "January 10, 2018, 10:40 PM"
    datetime_format = "%B %d, %Y, %I:%M %p"

    # create context for the email template
    context = {
        "task_id": task_id,
        "task_name": task_name,
        "task_description": task_description,
        "task_sketch_deadline": task_sketch_deadline.strftime(datetime_format),
        "task_link": task_link,
    }

    from_email, to = settings.EMAIL_HOST_USER, wizard_email
    if is_assignment:
        subject = "Assigned on new task"
        html_content = render_to_string('manage_tasks/email_templates/wizard_assigned.html', context)
    else:
        subject = "Removed from assigned task"
        html_content = render_to_string('manage_tasks/email_templates/wizard_removed.html', context)

    text_content = strip_tags(html_content)  # Strip the html tag. So people can see the pure text at least.

    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    is_sent = msg.send()

    # # get the appropriate email template that will be populated with necessary data
    # if is_assignment:
    #     email_template = get_template('manage_tasks/email_templates/wizard_assigned.html')
    # else:
    #     email_template = get_template('manage_tasks/email_templates/wizard_removed.html')
    #
    # content = email_template.render(context)
    #
    # # create an email and try to send it
    # if is_assignment:
    #     email = EmailMessage("Assigned on new task", content, settings.EMAIL_HOST_USER, [wizard_email])
    # else:
    #     email = EmailMessage("Removed from assigned task", content, settings.EMAIL_HOST_USER, [wizard_email])
    # email.content_subtype = "html"
    # is_sent = email.send()

    # log information needed to resend the email if email sending failed
    if not is_sent:
        # log information necessary to resend the email
        logger = logging.getLogger(__name__)

        if is_assignment:
            logger.error(
                "Email sending for wizard %s about assignment of task %s has failed.",
                wizard_email, task_id
            )
        else:
            logger.error(
                "Email sending for wizard %s about assignment removal of task %s has failed.",
                wizard_email, task_id
            )

        # insert all the necessary information to log the failure
        context["email"] = wizard_email
        context["is_assignment"] = is_assignment
        context["reason"] = "wizard_assignment_email"

        # log failure to a file in a way that contains necessary information to recover
        log_failure(context)


@celery_app.task
def judge_assignment(
        judge_email, is_assignment, task_id, task_name,
        task_description, task_sketch_deadline, task_link):
    """
    Sends an email to a judge to inform that he/she is
    assigned on a specific task asynchronously

    Args:
         judge_email: the email of judge that is recently assigned on a task
         is_assignment: whether the judge is being assigned on a task or removed from
         task_id: the id of the task that a judge is assigned on or removed from
         task_name: the name of the task
         task_description: the description of the task
         task_sketch_deadline: the sketch deadline of the task

    Returns:
        nothing, this function is asynchronous
    """

    # a format of "January 10, 2018, 10:40 PM"
    datetime_format = "%B %d, %Y, %I:%M %p"

    # create context for the email template
    context = {
        "task_id": task_id,
        "task_name": task_name,
        "task_description": task_description,
        "task_sketch_deadline": task_sketch_deadline.strftime(datetime_format),
        "task_link": task_link,
    }

    from_email, to = settings.EMAIL_HOST_USER, judge_email
    if is_assignment:
        subject = "Assigned on new task"
        html_content = render_to_string('manage_tasks/email_templates/judge_assigned.html', context)
    else:
        subject = "Removed from assigned task"
        html_content = render_to_string('manage_tasks/email_templates/judge_assigned.html', context)

    text_content = strip_tags(html_content)  # Strip the html tag. So people can see the pure text at least.

    # create the email, and attach the HTML version as well.
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    is_sent = msg.send()

    # # get the appropriate email template that will be populated with necessary data
    # if is_assignment:
    #     email_template = get_template('manage_tasks/email_templates/wizard_assigned.html')
    # else:
    #     email_template = get_template('manage_tasks/email_templates/wizard_removed.html')
    #
    # content = email_template.render(context)
    #
    # # create an email and try to send it
    # if is_assignment:
    #     email = EmailMessage("Assigned on new task", content, settings.EMAIL_HOST_USER, [wizard_email])
    # else:
    #     email = EmailMessage("Removed from assigned task", content, settings.EMAIL_HOST_USER, [wizard_email])
    # email.content_subtype = "html"
    # is_sent = email.send()

    # log information needed to resend the email if email sending failed
    if not is_sent:
        # log information necessary to resend the email
        logger = logging.getLogger(__name__)

        if is_assignment:
            logger.error(
                "Email sending for judge %s about assignment of task %s has failed.",
                judge_email, task_id
            )
        else:
            logger.error(
                "Email sending for wizard %s about assignment removal of task %s has failed.",
                judge_email, task_id
            )

        # insert all the necessary information to log the failure
        context["email"] = judge_email
        context["is_assignment"] = is_assignment
        context["reason"] = "judge_assignment_email"

        # log failure to a file in a way that contains necessary information to recover
        log_failure(context)
