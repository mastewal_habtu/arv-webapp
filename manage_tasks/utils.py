import random

from arv import settings
from manage_images.models import Image, RecentImage

import redis

"""
A global variable to represent redis connection
"""
r = redis.StrictRedis(host=settings.REDIS_HOST, port=settings.REDIS_PORT, db=settings.REDIS_DB)

"""
An image is considered to be used recently
if the image is used in last 'RECENT_USED_LIMIT' number of tasks
"""
RECENT_USAGE_LIMIT = 4

"""
The key to store and retrieve recent image ids in redis
"""
RECENT_IMAGES_KEY = "recent_images"


def get_2_random_task_images():
    """
    Get 2 randomly selected task images based on requirement

    Returns:
        two image instances as a tuple,
        if an image instance could not be found for any reason None will be used in its place
    """

    images_count = Image.objects.count()

    # if there is no enough task images return nothing
    if images_count < 1:
        return None, None

    # create a loop that get a random image that is not recently used
    # create  a threshold to break loop if we can't somehow get an image that is not recent
    threshold = images_count

    idx1 = random.randrange(0, images_count)
    image1 = Image.objects.all()[idx1]
    while is_recently_used(image1):  # get a random image that is not recently used
        idx1 = random.randrange(0, images_count)
        image1 = Image.objects.all()[idx1]

        threshold -= 1
        if threshold < 0:
            return None, None

    # get another image that is a potential pair that is not recently used
    potential_images = image1.potentials.all()
    for potential_image in potential_images:
        if not is_recently_used(potential_image):
            return image1, potential_image

    # check if at least one potential image exist
    potential_images_count = potential_images.count()
    if not potential_images_count > 0:
        return image1, None

    # since all potential images are recently used, pick a random one from them
    idx2 = random.randrange(0, potential_images_count)
    return image1, potential_images[idx2]


def is_recently_used(image):
    """
    Checks if an image is used recently

    Args:
        image: the image to be checked for its recent usage

    Returns:
        a boolean indicating if the image is used recently or not

    Assumes:
            Redis is setup appropriately and global variable 'r' represents it
    """

    return r.zscore(RECENT_IMAGES_KEY, image.id) is not None


def update_recently_used_images(*images):
    """
    Updates what task images has been used recently

    Args:
        *images: a set of task images that are used recently

    Assumes:
        Redis is setup appropriately and global variable 'r' represents it
    """

    for image in images:
        if is_recently_used(image):
            # if image already exists in recently used images,

            # use the score of the last item as the maximum score
            most_recent = r.zrange(RECENT_IMAGES_KEY, 0, -1)[-1]
            most_recent_score = r.zscore(RECENT_IMAGES_KEY, most_recent)
            # insert the new image id with the highest score to make it the last item
            r.zincrby(RECENT_IMAGES_KEY, image.id, most_recent_score + 1)

            continue

        # if we don't have enough number of objects that are not considered as recently used images
        #    we can insert a new image without deleting any image from recently used images
        recent_images_num = r.zcard(RECENT_IMAGES_KEY)
        if recent_images_num < RECENT_USAGE_LIMIT:
            r.zincrby(RECENT_IMAGES_KEY, image.id, recent_images_num + 1)
        else:
            # remove the first member
            r.zremrangebyrank(RECENT_IMAGES_KEY, 0, 0)

            # use the score of the last item as the maximum score
            most_recent = r.zrange(RECENT_IMAGES_KEY, 0, -1)[-1]
            most_recent_score = r.zscore(RECENT_IMAGES_KEY, most_recent)
            # insert the new image id with the highest score to make it the last item
            r.zincrby(RECENT_IMAGES_KEY, image.id, most_recent_score + 1)


# N.B. this method is deprecated and not used
def is_recently_used_using_db(image):
    """
    Checks if an image is used recently

    Args:
        image: the image to be checked for its recent usage

    Returns:
        a boolean indicating if the image is used recently or not
    """

    return RecentImage.objects.filter(image=image).exists()


# N.B. this method is deprecated and not used
def update_recently_used_images_using_db(*images):
    """
    Updates what task images has been used recently

    Args:
        *images: a set of task images that are used recently

    Assumes:
        Accessing the database as if it was a queue
    """

    for image in images:
        # if image already exists in recently used images,
        #    delete it and insert it since we are assuming the database as if it was a queue
        if is_recently_used(image):
            RecentImage.objects.get(image=image).delete()
            RecentImage.objects.create(image=image)
            continue
        # if we don't have enough number of objects that are not considered as recently used images
        #    we can insert a new image without deleting any image from recently used images
        if RecentImage.objects.count() < RECENT_USAGE_LIMIT:
            RecentImage.objects.create(image=image)
        else:  # else remove the first one and insert it since we are assuming the database as queue
            RecentImage.objects.first().delete()
            RecentImage.objects.create(image=image)
