import pytz
import random

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from core.forms import TimezoneForm
from core.utils import get_items_with_pages, get_paginated_items, get_pages, get_status, is_admin, ajax_required, \
    get_user_profile
from manage_tasks.forms import TaskForm, EditTaskForm
from manage_tasks.models import Task, WizardAssignment, JudgeAssignment
from manage_tasks.tasks import wizard_assignment, judge_assignment
from manage_tasks.utils import get_2_random_task_images, update_recently_used_images


@user_passes_test(is_admin)
def view_tasks(request):
    """
    A view to show all tasks

        tasks should be filtered somehow rather than listing all of them
    """
    tasks_list = Task.objects.all()

    tasks = get_paginated_items(request, tasks_list)
    pages = get_pages(tasks)

    context = {
        "tasks": tasks,
        "pages": pages
    }

    return render(request, "manage_tasks/view_tasks.html", context)


@user_passes_test(is_admin)
def create_task(request):
    """
    A view to create new task
    """

    if request.method == "POST":
        # get 2 random task images and assign them to the task
        image1, image2 = get_2_random_task_images()
        if image1 is None or image2 is None:
            # save the failure info if the task creating process was unsuccessful
            request.session['not_created'] = True
            # save the errors that could arise from getting random image
            if any((image1, image2)):
                request.session['other_errors'] = ["Unable to get a potential pair for an image. Please, try again"]
            else:
                request.session['other_errors'] = ["Unable to get random image. Please, try again"]

            return redirect(request.path)

        # initial form data for timezone to check if it has changed
        initial_data = {"timezone": timezone.get_current_timezone_name()}
        # populate form from submitted data
        task_form = TaskForm(request.POST)
        timezone_form = TimezoneForm(request.POST, instance=request.user.profile, initial=initial_data)
        # create new task from form and update timezone preference
        new_task = create_task_from_form(request, task_form, timezone_form)

        # if new task is created
        if new_task:
            new_task.image1 = image1
            new_task.image2 = image2

            # Todo: an algorithm to select the event image
            #       ( may be this step could be deferred to another time )
            #############################################################################
            # assign one of the images randomly as event image
            random_image_index = random.choice([Task.IMAGE_1, Task.IMAGE_2])
            new_task.event = random_image_index
            ##############################################################################

            new_task.save()

            # save the success info if task creating was successful
            request.session['task_created'] = True

            # update information about recent image usage appropriately
            update_recently_used_images(image1, image2)

            # redirect to page to assign participants to it
            return redirect("manage_tasks:edit_task", new_task.id)
        else:
            # save the failure info if the task creating process was unsuccessful
            request.session['not_created'] = True
            # save the errors that could arise from form submission
            request.session['task_form_errors'] = task_form.errors
            request.session['timezone_form_errors'] = timezone_form.errors

            # redirect to itself
            return redirect(request.path)

    else:  # if request method is GET
        # create new forms
        task_form = TaskForm()
        timezone_form = TimezoneForm()

    # check if the task creation has failed and invalidate it
    failure = get_status(request, 'not_created', False)
    # get found errors and invalidate them
    task_form_errors = get_status(request, "task_form_errors", {})
    timezone_form_errors = get_status(request, "timezone_form_errors", {})
    other_errors = get_status(request, "other_errors", [])

    # context variables for template
    context = {
        "task_form": task_form,
        "timezone_form": timezone_form,
        "timezones": pytz.common_timezones,
        "failure": failure,
        "task_form_errors": task_form_errors,
        "timezone_form_errors": timezone_form_errors,
        "other_errors": other_errors,
    }

    return render(request, "manage_tasks/create_task.html", context)


@user_passes_test(is_admin)
def edit_task(request, task_id):
    """
    A view to edit task
    """

    # get the selected task object
    task = get_object_or_404(Task, id=task_id)

    # wizards assigned to the selected task
    assigned_wizards = task.wizards.all()
    assigned_wizards_ids = assigned_wizards.values_list('pk', flat=True)

    # judges assigned to the selected task
    assigned_judges = task.judges.all()
    assigned_judges_ids = assigned_judges.values_list('pk', flat=True)

    # Get only wizards from all users
    wizards_list = User.objects.filter(groups__name__iexact="wizards")
    wizards = get_paginated_items(request, wizards_list)
    wizards_pages = get_pages(wizards)

    # get only judges from all users
    judges_list = User.objects.filter(groups__name__iexact="judges")
    judges = get_paginated_items(request, judges_list)
    judges_pages = get_pages(judges)

    # if form is submitted
    if request.method == "POST":
        # populate both forms from submitted data and current model
        task_form = EditTaskForm(request.POST, instance=task)

        is_updated = update_task_from_form(request, task_form)

        # save the success info if the update was successful
        request.session['updated'] = is_updated
        # save the failure info if the update was unsuccessful
        request.session['not_updated'] = not is_updated
        # save the errors that could arise from form submission
        request.session['task_form_errors'] = task_form.errors

        # redirect to itself
        return redirect(request.path)
    else:  # if request method is GET
        # populate form using current model
        task_form = EditTaskForm(instance=task)

    # check if there was successful update and invalidate it
    success = get_status(request, 'updated', False)
    # check if there was unsuccessful update and invalidate it
    failure = get_status(request, 'not_updated', False)
    # get found errors and invalidate them
    task_form_errors = get_status(request, "task_form_errors", {})

    # context variables for template
    context = {
        "task": task,
        "task_form": task_form,
        "wizards": wizards,
        "wizards_pages": wizards_pages,
        "assigned_wizards": assigned_wizards,
        "assigned_wizards_ids": assigned_wizards_ids,
        "judges": judges,
        "judges_pages": judges_pages,
        "assigned_judges": assigned_judges,
        "assigned_judges_ids": assigned_judges_ids,
        "success": success,
        "failure": failure,
        "task_form_errors": task_form_errors,
    }

    return render(request, "manage_tasks/edit_task.html", context)


@ajax_required
@user_passes_test(is_admin)
def delete_task(request):
    """
    A view to delete a task

    Todo: I don't know if this is necessary at all
    """

    task_id = int(request.POST['item_id'])
    task = get_object_or_404(Task, id=task_id)
    task.delete()

    return HttpResponse("Deleted Successfully")


@ajax_required
@user_passes_test(is_admin)
def get_filtered_tasks(request):
    """
    Return a table of tasks filtered by submitted criteria
    """

    # get the task attribute to be filtered
    attribute = request.GET.get("attribute")
    # get the search term
    search_term = request.GET.get("search_term").strip()

    if len(search_term) == 0:
        # we don't need filtering criteria
        kwargs = {}
    else:
        # construct keyword args for field look up
        kwargs = {
            "{0}__{1}".format(attribute, "icontains"): search_term,
        }

    tasks_list = Task.objects.filter(**kwargs)

    tasks = get_paginated_items(request, tasks_list)
    pages = get_pages(tasks)

    #  context variables for template
    context = {
        "tasks": tasks,
        "pages": pages,
    }

    return render(request, "manage_tasks/ajax_responses/filtered_tasks_table.html", context)


@ajax_required
@user_passes_test(is_admin)
def get_filtered_wizards(request, task_id):
    """
    Return a table of wizards filtered by submitted criteria
    and uses the ids of the assigned wizards to differentiate
    who is assigned as wizard on the selected task

    Args:
        request: the http request
        task_id: the currently selected task
    """

    # get the user attribute to be filtered
    attribute = request.GET.get("attribute")
    # get the search term
    search_term = request.GET.get("search_term").strip()

    # make sure the users are wizards
    kwargs = {
        "{0}__{1}__{2}".format("groups", "name", "iexact"): "wizards"
    }

    if len(search_term) > 0:
        # if attribute is user attribute
        if hasattr(request.user, attribute):
            # construct keyword args for field look up
            kwargs = {
                "{0}__{1}".format(attribute, "icontains"): search_term,
            }
        # if attribute is profile attribute
        elif hasattr(request.user.profile, attribute):
            # construct keyword args for field look up
            kwargs = {
                "{0}__{1}__{2}".format("profile", attribute, "icontains"): search_term,
            }

    # get users based on kwargs
    wizards_list = User.objects.filter(**kwargs)

    wizards = get_paginated_items(request, wizards_list)
    pages = get_pages(wizards)

    # wizards assigned to the selected task
    task = get_object_or_404(Task, id=task_id)
    assigned_wizards = task.wizards.all()
    assigned_wizards_ids = assigned_wizards.values_list('pk', flat=True)

    context = {
        "wizards": wizards,
        "pages": pages,
        "assigned_wizards_ids": assigned_wizards_ids,
    }

    return render(request, "manage_tasks/ajax_responses/filtered_wizards_table.html", context)


@ajax_required
@user_passes_test(is_admin)
def get_filtered_judges(request, task_id):
    """
    Return a table of judges filtered by submitted criteria
    and uses the ids of the assigned judges to differentiate
    who is assigned as judge on the selected task

    Args:
        request: the http request
        task_id: the currently selected task
    """

    # get the user attribute to be filtered
    attribute = request.GET.get("attribute")
    # get the search term
    search_term = request.GET.get("search_term").strip()

    # make sure the users are wizards
    kwargs = {
        "{0}__{1}__{2}".format("groups", "name", "iexact"): "judges",
    }

    if len(search_term) > 0:
    # if attribute is user attribute
        if hasattr(request.user, attribute):
            # construct keyword args for field look up
            kwargs = {
                "{0}__{1}".format(attribute, "icontains"): search_term,
            }
        # if attribute is profile attribute
        elif hasattr(request.user.profile, attribute):
            # construct keyword args for field look up
            kwargs = {
                "{0}__{1}__{2}".format("profile", attribute, "icontains"): search_term,
            }

    # get users based on kwargs
    judges_list = User.objects.filter(**kwargs)

    judges = get_paginated_items(request, judges_list)
    pages = get_pages(judges)

    # wizards assigned to the selected task
    task = get_object_or_404(Task, id=task_id)
    assigned_judges = task.judges.all()
    assigned_judges_ids = assigned_judges.values_list('pk', flat=True)

    context = {
        "judges": judges,
        "pages": pages,
        "assigned_judges_ids": assigned_judges_ids,
    }

    return render(request, "manage_tasks/ajax_responses/filtered_judges_table.html", context)


########################################################################################
# Helper functions
########################################################################################

def create_task_from_form(request, task_form, timezone_form):
    """
    A logic to create a new task from form data

    Args:
        request: the http request
        task_form: user data populated TaskForm instance
        timezone_form: user data or initial data populated TimezoneForm instance

    Returns:
        the new created task without being saved to the database or None if a task could not be created
    """

    # if valid timezone is set, update timezone preference
    if timezone_form.is_valid():
        if timezone_form.has_changed():
            # update timezone preference
            timezone_form.save()
            # set current timezone based on selection
            timezone.activate(pytz.timezone(timezone_form.cleaned_data['timezone']))

        # if form is valid, create new task
        if task_form.is_valid():
            # create task from form
            new_task = task_form.save(commit=False)

            return new_task

    # if the form is invalid
    return None


def update_task_from_form(request, task_form):
    """
    A logic to update a specific task based on submitted form data

    Args:
        request: the http request
        task_form: the user data populated EditTaskForm instance

    Returns:
        a boolean indicating if the task is updated or not
    """

    # if forms are valid, update the task
    if task_form.is_valid():
        if task_form.has_changed():
            task_form.save()

        return True

    return False


##################################################################################
# Helper Functions to Respond Ajax Requests of Assigning or Removing Users       #
# from a Task                                                                    #
##################################################################################


@ajax_required
@user_passes_test(is_admin)
def assign_remove_wizards(request):
    """
    Assign or Remove wizards to a specific task
    """

    # get submitted data
    task_id = int(request.POST.get("task_id"))
    wizard_id = int(request.POST.get("user_id"))

    # get model objects
    task = get_object_or_404(Task, pk=task_id)
    wizard = get_object_or_404(User, pk=wizard_id)

    # assign or remove wizard based on request
    is_assignment = request.POST.get("assignment")
    if is_assignment == "true":
        assignment = WizardAssignment(task=task, wizard=wizard)
        assignment.save()

        # increase the number of tasks that a user is actively doing
        user_profile = get_user_profile(wizard)
        user_profile.active_tasks += 1
        user_profile.save()

        # build an absolute url to the task that will be used in email
        task_link = request.build_absolute_uri(reverse('wizards:unfinished_tasks'))

        # send an email notification to the user
        wizard_assignment.delay(
            wizard.email, True, task.id, task.name,
            task.description, task.sketch_deadline, task_link)
    else:
        assignment = WizardAssignment.objects.get(task=task, wizard=wizard)
        assignment.delete()

        # decrement the number of tasks that a user is actively doing
        user_profile = get_user_profile(wizard)
        user_profile.active_tasks -= 1
        user_profile.save()

        # build an absolute url to the task that will be used in email
        task_link = request.build_absolute_uri(reverse('wizards:unfinished_tasks'))

        # send an email notification to the user
        wizard_assignment.delay(
            wizard.email, False, task.id, task.name,
            task.description, task.sketch_deadline, task_link)

    return HttpResponse("Processed Successfully")


@ajax_required
@user_passes_test(is_admin)
def assign_remove_judges(request):
    """
    Assign or remove judges to a specific task
    """

    # get submitted data
    task_id = int(request.POST.get("task_id"))
    judge_id = int(request.POST.get("user_id"))

    # get model objects
    task = get_object_or_404(Task, pk=task_id)
    judge = get_object_or_404(User, pk=judge_id)

    # assign or remove judge based on request
    is_assignment = request.POST.get("assignment")
    if is_assignment == "true":
        assignment = JudgeAssignment(task=task, judge=judge)
        assignment.save()

        # increase the number of tasks that a user is actively doing
        user_profile = get_user_profile(judge)
        user_profile.active_tasks += 1
        user_profile.save()

        # build an absolute url to the task that will be used in email
        task_link = request.build_absolute_uri(reverse('judges:unfinished_tasks'))

        # send an email notification to the user
        judge_assignment.delay(
            judge.email, True, task.id, task.name,
            task.description, task.sketch_deadline, task_link)
    else:
        assignment = JudgeAssignment.objects.get(task=task, judge=judge)
        assignment.delete()

        # decrement the number of tasks that a user is actively doing
        user_profile = get_user_profile(judge)
        user_profile.active_tasks -= 1
        user_profile.save()

        # build an absolute url to the task that will be used in email
        task_link = request.build_absolute_uri(reverse('judges:unfinished_tasks'))

        # send an email notification to the user
        judge_assignment.delay(
            judge.email, False, task.id, task.name,
            task.description, task.sketch_deadline, task_link)

    return HttpResponse("Processed Successfully")
