from django.apps import AppConfig


class ManageTasksConfig(AppConfig):
    name = 'manage_tasks'
