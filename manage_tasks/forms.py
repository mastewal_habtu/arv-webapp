from django.forms import ModelForm
from django.utils import timezone
from django import forms

from .models import Task

from arv import settings


class TaskForm(ModelForm):
    """
    Form for creating new task
    """

    def __init__(self, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    # just to limit the times based on datetime formats specified in the settings
    sketch_deadline = forms.DateTimeField(input_formats=settings.DATETIME_INPUT_FORMATS)
    judgment_deadline = forms.DateTimeField(input_formats=settings.DATETIME_INPUT_FORMATS)
    trade_start = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
    trade_exit = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

    class Meta:
        model = Task
        fields = ['name', 'description', 'sketch_deadline', 'judgment_deadline', 'trade_start', 'trade_exit']

    def clean_sketch_deadline(self):
        """
        Checks if sketch deadline is a datetime in the future

        Raises:
            form validation error
        """

        sketch_deadline = self.cleaned_data['sketch_deadline']
        if sketch_deadline <= timezone.localtime(timezone.now(), timezone.get_current_timezone()):
            raise forms.ValidationError("Sketch deadline cannot be in the past.")

        return sketch_deadline

    def clean_judgment_deadline(self):
        """
        Checks if judgment deadline is not prior to sketch deadline

        Raises:
            form validation error
        """

        judgment_deadline = self.cleaned_data['judgment_deadline']
        sketch_deadline = self.cleaned_data['sketch_deadline']
        if judgment_deadline <= sketch_deadline:
            raise forms.ValidationError("Judgment deadline cannot be before sketch deadline.")

        return judgment_deadline

    def clean_trade_exit(self):
        """
        Checks if trade exit time is not prior to trade start time

        Raises:
            form validation error
        """

        trade_exit = self.cleaned_data['trade_exit']
        trade_start = self.cleaned_data['trade_start']
        if trade_exit <= trade_start:
            raise forms.ValidationError("Trade exit time cannot be before trade start time.")

        return trade_exit


class EditTaskForm(ModelForm):
    """
    Form for editing a task
    """

    def __init__(self, *args, **kwargs):
        super(EditTaskForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            if 'required' in field.error_messages:
                field.error_messages['required'] = '{0} is required'.format(key)

    class Meta:
        model = Task
        fields = ['name', 'description', ]
