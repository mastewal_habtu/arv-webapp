from django.conf import settings
from django.db import models

from arv.settings import WIZARD_TASK_IMAGES_FOLDER
import judges
from manage_images.models import Image

import pytz


"""
all timezones from pytz library as pairs
"""
all_timezones = zip(pytz.all_timezones, pytz.all_timezones)


class Task(models.Model):
    """
    Model Task to represent a specific task
    """

    # possible states of a task
    ASSIGNED_NEW = 1
    UNFINISHED = 2
    FINISHED = 3
    FEEDEDBACK_NEW = 4
    FEEDEDBACK = 5
    EXPIRED_NEW = 6
    EXPIRED = 7

    # possible states with their corresponding human friendly description
    states = [
        (ASSIGNED_NEW, "New Task"),
        (UNFINISHED, "Unfinished Task"),
        (FINISHED, "Finished Task"),
        (FEEDEDBACK_NEW, "New Feedback"),
        (FEEDEDBACK, "Finished Task(with feedback)"),
        (EXPIRED_NEW, "New Expired Task"),
        (EXPIRED, "Expired Task"),
    ]

    # possible event image modes
    NO_EVENT = 0
    IMAGE_1 = 1
    IMAGE_2 = 2

    # possible events with their corresponding human friendly description
    events = [
        (NO_EVENT, "No Event"),
        (IMAGE_1, "Image 1"),
        (IMAGE_2, "Image 2"),
    ]

    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)

    # datetime deadlines
    sketch_deadline = models.DateTimeField()
    judgment_deadline = models.DateTimeField()
    trade_start = models.DateField()
    trade_exit = models.DateField()

    # people assigned to task
    wizards = models.ManyToManyField(settings.AUTH_USER_MODEL, through="manage_tasks.WizardAssignment", related_name="wizard_tasks")
    judges = models.ManyToManyField(settings.AUTH_USER_MODEL, through="manage_tasks.JudgeAssignment", related_name="judge_tasks")

    # images assigned to task
    image1 = models.ForeignKey(Image, related_name="tasks_image1", blank=True, null=True, on_delete=models.CASCADE)
    image2 = models.ForeignKey(Image, related_name="tasks_image2", blank=True, null=True, on_delete=models.CASCADE)

    # which image happened to be the event image
    event = models.SmallIntegerField(choices=events, default=NO_EVENT)

    # get the image associated with the happened event
    def get_event_image(self):
        if self.event:
            if self.event == self.IMAGE_1:
                return self.image1
            elif self.event == self.IMAGE_2:
                return self.image2

        return None

    # get the image favored by the assigned judges
    def get_favored_image(self):
        if JudgeAssignment.objects.filter(task=self,state__lte=Task.UNFINISHED).exists():
            return None

        judgments = judges.models.Judgment.objects.filter(wizardassignment__task=self, state__gte=Task.FINISHED)
        rank_1, rank_2 = 0, 0
        for judgment in judgments:
            rank_1 += judgment.rank_1 if judgment.rank_1 else 0
            rank_2 += judgment.rank_2 if judgment.rank_2 else 0

        if rank_1 > rank_2:
            return self.image1
        elif rank_2 > rank_1:
            return self.image2

        return None


    def __str__(self):
        return self.name or self.description or super().__str__()


class WizardAssignment(models.Model):
    """
    A through table model for wizard and task relationship of assignment
    """

    wizard = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="wizard_assignment")
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    state = models.SmallIntegerField(choices=Task.states, default=Task.ASSIGNED_NEW)

    # the image uploaded for the specified task
    wizard_image = models.ImageField(upload_to=WIZARD_TASK_IMAGES_FOLDER, blank=True)

    class Meta:
        unique_together = ('wizard', 'task')

    def __str__(self):
        return self.wizard.username + " on " + self.task.name

    def is_correct(self):
        if self.task.event is None:
            return None

        judgments = judges.models.Judgment.objects.filter(wizardassignment=self)
        rank_1, rank_2 = 0, 0
        for judgment in judgments:
            rank_1 += judgment.rank_1 if judgment.rank_1 else 0
            rank_2 += judgment.rank_2 if judgment.rank_2 else 0

        if rank_1 > rank_2:
            return self.task.event == Task.IMAGE_1
        elif rank_2 > rank_1:
            return self.task.event == Task.IMAGE_2

        return None


class JudgeAssignment(models.Model):
    """
    A through table model for judge and task relationship of assignment
    """

    judge = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="judge_assignment")
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    state = models.SmallIntegerField(choices=Task.states, default=Task.ASSIGNED_NEW)

    class Meta:
        unique_together = ('judge', 'task')

    def __str__(self):
        return self.judge.username + " on " + self.task.name

    def get_feedback(self):
        judgments = judges.models.Judgment.objects.filter(judge=self.judge, wizardassignment__task=self.task)

        correct, incorrect, total = 0, 0, 0
        for judgment in judgments:
            total += 1
            is_correct = judgment.is_correct()
            if is_correct is not None:
                if is_correct:
                    correct += 1
                else:
                    incorrect += 1

        return [correct, incorrect, total]
