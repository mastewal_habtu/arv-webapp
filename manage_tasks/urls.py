from django.conf.urls import url

from manage_tasks import views as task_views

app_name = 'manage_tasks'
urlpatterns = [
    # urls that are associated with managing tasks
    url(r'^create_task/$', task_views.create_task, name="create_task"),
    url(r'^view_tasks/$', task_views.view_tasks, name="view_tasks"),
    url(r'^edit_task/(?P<task_id>\d+)/$', task_views.edit_task, name="edit_task"),
    url(r'^delete_task/$', task_views.delete_task, name="delete_task"),
    url(r'^get_filtered_tasks/$', task_views.get_filtered_tasks, name="get_filtered_tasks"),
    url(r'^get_filtered_wizards/(?P<task_id>\d+)/$', task_views.get_filtered_wizards, name="get_filtered_wizards"),
    url(r'^get_filtered_judges/(?P<task_id>\d+)/$', task_views.get_filtered_judges, name="get_filtered_judges"),
    url(r'^assign_remove_viewers/$', task_views.assign_remove_wizards, name="assign_remove_viewers"),
    url(r'^assign_remove_judges/$', task_views.assign_remove_judges, name="assign_remove_judges"),
 
]

