# Read Me

### Requirements:
* Python3 or above
* Django 2.0 or above
* Required python packages
    1. mysqlclient (if mysql database is used)
    1. Pillow
    2. sorl-thumbnail
    3. redis
    4. celery(depends on redis)
* Redis server(needs to be started with the same configuration as `arv/settings`)
* Celery (needs to be started with the same configuration as `arv/settings`)
* MySQL recent version
(Actually, you can use any django supported database by modifying `arv/settings` a little bit)

### Steps Required to start the web app:
1. Set database name, username and password in file `arv/settings.py`
3. Start mysql if not started with same configuration as specified in `arv/settings.py`
3. Start redis server if not started with same configuration as specified in `arv/settings.py`
3. Start celery if not started with same configuration as specified in `arv/settings.py`
2. Create the necessary migrations by running the following command
    `python3 manage.py makemigrations`
3. Migrate the created migrations by running the following command
    `python3 manage.py migrate`
1. Create the necessary groups by running the following command
    `python3 manage.py init_groups`
4. Create superuser by running the following command
    `python3 manage.py createsuperuser`
5. Log in with superuser credential using `/admin` url
6. Create new user and add the user into Judges group(serve as Admin group too) using the provided interface
7. Finally, you can log in using the new created user credential with `/` url