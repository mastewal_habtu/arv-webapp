from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseBadRequest

from manage_tasks.models import Task, WizardAssignment
from manage_images.models import Image
from core.utils import get_paginated_items, get_pages, get_user_profile
from core.forms import ImageForm

import random


# check if user is wizard and also has logged in
def is_wizard(user):
    if user.is_authenticated:
        return user.groups.filter(name='wizards').exists()
    else:
        return False


#####################################################################
# View Related to Managing Tasks                                    #
#####################################################################

# view to show unfinished tasks
@user_passes_test(is_wizard)
def view_unfinished_tasks(request):
    # get tasks that are unfinished and
    # order them based on whether they are new or not
    tasks_list = request.user.wizard_tasks.order_by('wizardassignment__state') \
        .filter(wizardassignment__state__lt=Task.FINISHED)
    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    # change the states of new task to unfinished
    assignments = []
    for task in tasks:
        wizard_assignment = get_object_or_404(WizardAssignment, wizard=request.user, task=task)
        assignments.append(wizard_assignment.state)
        if wizard_assignment.state == Task.ASSIGNED_NEW:
            wizard_assignment.state = Task.UNFINISHED
            wizard_assignment.save()

    # zip tasks with their corresponding wizard assignments
    zipped_tasks = zip(tasks, assignments)

    # context variable for template
    context = {
        "zipped_tasks": zipped_tasks,
        "pages": get_pages(tasks),
        "tasks": tasks,

        # constant
        "ASSIGNED_NEW": Task.ASSIGNED_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "wizards/view_unfinished_tasks.html", context)


# view to finished tasks
@user_passes_test(is_wizard)
def view_finished_tasks(request):
    # get tasks that are finished and
    # order them based on whether they have new feedback or not
    tasks_list = request.user.wizard_tasks.order_by('wizardassignment__state') \
        .filter(wizardassignment__state__gte=Task.FINISHED) \
        .exclude(wizardassignment__state__gte=Task.EXPIRED_NEW)
    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks
    new_info_count_context = get_new_info_count(request.user)

    wizard_assignments = []
    # change the states of new task to unfinished
    for task in tasks:
        assignment = get_object_or_404(WizardAssignment, wizard=request.user, task=task)
        wizard_assignments.append(assignment)
        if assignment.state == Task.FEEDEDBACK_NEW:
            assignment.state = Task.FEEDEDBACK
            assignment.save()

    # zip tasks with their assignments
    task_assignments = zip(tasks, wizard_assignments)

    # context variable for template
    context = {
        "task_assignments": task_assignments,
        "pages": get_pages(tasks),
        "tasks": tasks,

        # constants
        "FEEDEDBACK_NEW": Task.FEEDEDBACK_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "wizards/view_finished_tasks.html", context)


# view to finished tasks
@user_passes_test(is_wizard)
def view_expired_tasks(request):
    # get tasks that are finished and
    # order them based on whether they are new or not
    tasks_list = request.user.wizard_tasks \
        .filter(wizardassignment__state__gte=Task.EXPIRED_NEW) \
        .order_by('wizardassignment__state')
    tasks = get_paginated_items(request, tasks_list)

    # get the count of new tasks and new feedbacks before it is changed
    new_info_count_context = get_new_info_count(request.user)

    # change the states of new task to unfinished
    assignment_states = []
    for task in tasks:
        assignment = get_object_or_404(WizardAssignment, wizard=request.user, task=task)
        assignment_states.append(assignment.state)
        if assignment.state == Task.EXPIRED_NEW:
            assignment.state = Task.EXPIRED
            assignment.save()

    # zip tasks with their assignment states
    zipped_tasks = zip(tasks, assignment_states)

    # context variable for template
    context = {
        "zipped_tasks": zipped_tasks,
        "pages": get_pages(tasks),
        "tasks": tasks,

        # constants
        "EXPIRED_NEW": Task.EXPIRED_NEW,
    }

    # update the context variable which will be passed to template
    context.update(new_info_count_context)

    return render(request, "wizards/view_expired_tasks.html", context)


# get the number of new tasks assigned and new feedbacks to finished tasks
def get_new_info_count(wizard):
    # all finished tasks to a user
    unfinished_tasks = wizard.wizard_tasks.filter(wizardassignment__state__lt=Task.FINISHED)
    unfinished_tasks_count = unfinished_tasks.count()

    # new assigned tasks to a user
    new_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.ASSIGNED_NEW)
    new_tasks_count = new_tasks.count()

    # new feededback tasks to a user
    new_feededback_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.FEEDEDBACK_NEW)
    new_feededback_tasks_count = new_feededback_tasks.count()

    # new expired tasks to a user
    new_expired_tasks = wizard.wizard_tasks.filter(wizardassignment__state=Task.EXPIRED_NEW)
    new_expired_tasks_count = new_expired_tasks.count()

    # context that contains count info to be used in template
    context = {
        "unfinished_tasks_count": unfinished_tasks_count,
        "new_tasks_count": new_tasks_count,
        "new_feededback_tasks_count": new_feededback_tasks_count,
        "new_expired_tasks_count": new_expired_tasks_count,
    }

    return context


# upload wizard picture for a task making it finished
# (for ajax use only)
@user_passes_test(is_wizard)
def upload_wizard_picture(request):
    # if form is submitted using post method
    if request.method == "POST":
        image_form = ImageForm(request.POST, request.FILES)
        if image_form.is_valid():
            # get ids submitted to specify the models related
            wizard_id = int(request.POST["wizard_id"])
            task_id = int(request.POST["task_id"])

            # get the models related using the submitted ids
            wizard = get_object_or_404(User, pk=wizard_id)
            task = get_object_or_404(Task, pk=task_id)

            # get through table model
            wizard_assignment = get_object_or_404(
                WizardAssignment, wizard=wizard_id, task=task_id)
            # update through table model uploading image
            wizard_assignment.wizard_image = image_form.cleaned_data['image']

            # change status of the task for the wizard
            wizard_assignment.state = Task.FINISHED
            wizard_assignment.save()

            # increase the number of tasks that a user has finished
            user_profile = get_user_profile(wizard)
            user_profile.finished_tasks += 1
            user_profile.save()

            return HttpResponse("Image Uploaded Successfully")

    return HttpResponseBadRequest("Image Uploading Failed")


##############################################################################
# Views Related to Managing Practice                                         #
##############################################################################

# view to solo practice
@user_passes_test(is_wizard)
def view_solo_practice(request):
    # Hmm... amazing that this doesn't need any code.

    return render(request, "wizards/view_solo_practice.html")


# get random image for solo practice
# (for ajax use only)
@user_passes_test(is_wizard)
def get_random_image_url(request):
    # get random index number to choose image
    random_index = random.randrange(0, Image.objects.count())

    # get random image url using the random index
    random_image = Image.objects.all()[random_index]

    return HttpResponse(random_image.image.url)
