from django.conf.urls import url

from . import views

app_name = 'wizards'

urlpatterns = [
    url(r'^unfinished_tasks/$', views.view_unfinished_tasks, name="unfinished_tasks"),
    url(r'^finished_tasks/$', views.view_finished_tasks, name="finished_tasks"),
    url(r'^expired_tasks/$', views.view_expired_tasks, name="expired_tasks"),


    url(r'^upload_wizard_picture/$', views.upload_wizard_picture, name="upload_wizard_picture"),

    # deactivate unwanted feature which is solo practice for remote viewing
    # url(r'^solo_practice/$', views.view_solo_practice, name="solo_practice"),
    # url(r'^get_random_image_url/$', views.get_random_image_url, name="get_random_image_url"),
]

