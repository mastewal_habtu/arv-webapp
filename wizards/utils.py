import random

from core.models import TaskImage
from django.http import HttpResponse

# get a random image url for wizard
def get_random_image_url():
    # get random index number to choose image
    random_index = random.randrange(0, TaskImage.objects.count())

    # get random image url using the random index
    random_image = TaskImage.objects.all()[random_index]

    return HttpResponse(random_image.image.url)